-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2016 at 10:42 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dig_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_account_user`
--

CREATE TABLE `dig_core_account_user` (
  `id` int(32) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `createpassword` varchar(64) DEFAULT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `schoolid` int(16) NOT NULL,
  `avatar` text,
  `lang` text,
  `theme` text,
  `timezone` text,
  `city` text,
  `country` text,
  `middlename` text,
  `alternatename` text,
  `title` text,
  `isStaff` TINYINT DEFAULT 0,
  `accessToken` varchar(32) NOT NULL,
  `authKey` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_account_user_log`
--

CREATE TABLE `dig_core_account_user_log` (
  `userid` int(16) NOT NULL,
  `time` datetime NOT NULL,
  `operation` text NOT NULL,
  `IPaddress` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_artical_catagroy`
--

CREATE TABLE `dig_core_artical_catagroy` (
  `id` int(64) NOT NULL,
  `fullname` varchar(64) NOT NULL,
  `description` int(11) NOT NULL,
  `descriptionformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `timecreated` datetime NOT NULL,
  `timemodified` datetime NOT NULL,
  `usermodified` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_attachments`
--

CREATE TABLE `dig_core_attachments` (
  `id` int(255) NOT NULL,
  `path` text NOT NULL,
  `filetype` varchar(64) NOT NULL,
  `belong` varchar(64) NOT NULL,
  `parent` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_discussion`
--

CREATE TABLE `dig_core_discussion` (
  `id` int(255) NOT NULL,
  `belong` varchar(64) NOT NULL,
  `parent` int(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `subject` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `messageformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `messagetrust` int(11) NOT NULL,
  `canbereply` varchar(64) NOT NULL DEFAULT 'YES',
  `attachment` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_news`
--

CREATE TABLE `dig_core_news` (
  `id` int(255) NOT NULL,
  `catagroyid` int(64) NOT NULL,
  `userid` int(64) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `subject` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `messageformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `messagetrust` int(11) NOT NULL,
  `canreply` varchar(64) NOT NULL DEFAULT 'YES',
  `attachment` int(64) NOT NULL,
  `readercount` int(32) NOT NULL,
  `status` varchar(64) NOT NULL DEFAULT 'VISIABLE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_project`
--

CREATE TABLE `dig_core_project` (
  `id` int(64) NOT NULL,
  `fullname` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `descriptionformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `location` varchar(64) NOT NULL,
  `catagroyid` int(64) NOT NULL,
  `ownerid` int(64) NOT NULL,
  `path` text NOT NULL,
  `timecreated` datetime NOT NULL,
  `timemodified` datetime NOT NULL,
  `usermodified` int(64) NOT NULL,
  `status` varchar(64) NOT NULL DEFAULT 'VISIABLE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_project_catagroy`
--

CREATE TABLE `dig_core_project_catagroy` (
  `id` int(64) NOT NULL,
  `fullname` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `descriptionformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `timecreated` datetime NOT NULL,
  `timemodified` datetime NOT NULL,
  `usermodified` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dig_core_project_resource`
--

CREATE TABLE `dig_core_project_resource` (
  `id` int(11) NOT NULL,
  `belong` varchar(64) NOT NULL DEFAULT 'PROJECT',
  `parentid` int(11) NOT NULL,
  `path` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `size` int(11) NOT NULL,
  `status` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dig_core_account_user`
--
ALTER TABLE `dig_core_account_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_3` (`id`);

--
-- Indexes for table `dig_core_artical_catagroy`
--
ALTER TABLE `dig_core_artical_catagroy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dig_core_attachments`
--
ALTER TABLE `dig_core_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dig_core_discussion`
--
ALTER TABLE `dig_core_discussion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dig_core_news`
--
ALTER TABLE `dig_core_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dig_core_project`
--
ALTER TABLE `dig_core_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dig_core_project_catagroy`
--
ALTER TABLE `dig_core_project_catagroy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dig_core_project_resource`
--
ALTER TABLE `dig_core_project_resource`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dig_core_account_user`
--
ALTER TABLE `dig_core_account_user`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_artical_catagroy`
--
ALTER TABLE `dig_core_artical_catagroy`
  MODIFY `id` int(64) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_attachments`
--
ALTER TABLE `dig_core_attachments`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_discussion`
--
ALTER TABLE `dig_core_discussion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_news`
--
ALTER TABLE `dig_core_news`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_project`
--
ALTER TABLE `dig_core_project`
  MODIFY `id` int(64) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_project_catagroy`
--
ALTER TABLE `dig_core_project_catagroy`
  MODIFY `id` int(64) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dig_core_project_resource`
--
ALTER TABLE `dig_core_project_resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
