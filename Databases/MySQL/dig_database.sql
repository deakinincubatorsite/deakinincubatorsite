-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-10-03 17:39:25
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dig_database`
--

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_account_staff`
--

CREATE TABLE IF NOT EXISTS `dig_core_account_staff` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `schoolid` int(16) DEFAULT NULL,
  `avator` varchar(64) DEFAULT NULL,
  `lang` text,
  `theme` text,
  `timezone` text,
  `accessToken` varchar(32) NOT NULL,
  `authKey` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_account_user`
--

CREATE TABLE IF NOT EXISTS `dig_core_account_user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `createpassword` varchar(64) DEFAULT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `schoolid` int(16) NOT NULL,
  `avatar` text,
  `lang` text,
  `theme` text,
  `timezone` text,
  `city` text,
  `country` text,
  `middlename` text,
  `alternatename` text,
  `title` text,
  `isStaff` TINYINT DEFAULT 0,
  `accessToken` varchar(32) NOT NULL,
  `authKey` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_account_user_log`
--

CREATE TABLE IF NOT EXISTS `dig_core_account_user_log` (
  `userid` int(16) NOT NULL,
  `time` datetime NOT NULL,
  `operation` text NOT NULL,
  `IPaddress` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_attachments`
--

CREATE TABLE IF NOT EXISTS `dig_core_attachments` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `path` text NOT NULL,
  `filetype` varchar(64) NOT NULL,
  `belong` varchar(64) NOT NULL,
  `parent` int(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_discussion`
--

CREATE TABLE IF NOT EXISTS `dig_core_discussion` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `belong` varchar(64) NOT NULL,
  `parent` int(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `subject` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `messageformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `messagetrust` int(11) DEFAULT NULL,
  `canbereply` varchar(64) DEFAULT 'YES',
  `attachment` int(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- 转存表中的数据 `dig_core_discussion`
--

INSERT INTO `dig_core_discussion` (`id`, `belong`, `parent`, `userid`, `created`, `modified`, `subject`, `message`, `messageformat`, `messagetrust`, `canbereply`, `attachment`) VALUES
(71, 'forum', 0, 1, '2016-10-04 00:54:24', '2016-10-04 00:54:24', 'Project', 'The forum for project', 'HTML', NULL, NULL, NULL),
(72, 'topic', 71, 1, '2016-10-04 01:21:27', '2016-10-04 01:21:27', 'Project Descriptions and Discussion Contains unread posts', 'Projects lists on DIG will be reposted here.', 'HTML', NULL, NULL, NULL),
(73, 'forum', 0, 1, '2016-10-04 01:37:36', '2016-10-04 01:37:36', 'Questions and Feedback', 'As this is a new program, we want feedback from you about any issues you spot or questions you have.  Please post them here.', 'HTML', NULL, NULL, NULL),
(74, 'topic', 73, 1, '2016-10-04 01:37:56', '2016-10-04 01:37:56', 'DIG T3 Meeting- 30/9/15', 'Hi All,\r\n\r\nI would love to have gone to or Skyped into the meeting this Wednesday, however as I am working full time as I study Cloud, I unfortunately wont be finished work in time for this meeting.\r\n\r\nWill this meeting be recorded for me to look over in my own time?\r\n\r\nKind regards,\r\n\r\nChris.', 'HTML', NULL, NULL, NULL),
(75, 'topic', 74, 1, '2016-10-04 01:58:38', '2016-10-04 01:58:38', 'subject', 'Hi All,\r\n\r\nI would love to have gone to or Skyped into the meeting this Wednesday, however as I am working full time as I study Cloud, I unfortunately wont be finished work in time for this meeting.\r\n\r\nWill this meeting be recorded for me to look over in my own time?\r\n\r\nKind regards,\r\n\r\nChris.', 'HTML', NULL, NULL, NULL),
(76, 'topic', 74, 1, '2016-10-04 01:59:17', '2016-10-04 01:59:17', 'subject', 'Hi All,\r\n\r\nI would love to have gone to or Skyped into the meeting this Wednesday, however as I am working full time as I study Cloud, I unfortunately wont be finished work in time for this meeting.\r\n\r\nWill this meeting be recorded for me to look over in my own time?\r\n\r\nKind regards,\r\n\r\nChris.', 'HTML', NULL, NULL, NULL),
(77, 'topic', 75, 1, '2016-10-04 02:04:00', '2016-10-04 02:04:00', 'subject', 'I am interested in the answer to this question.\r\n', 'HTML', NULL, NULL, NULL),
(78, 'topic', 73, 1, '2016-10-04 02:04:52', '2016-10-04 02:04:52', 'Can someone I know from outside of Deakin join the DIG?', 'I have a friend who is studying in a university in Hong Kong at the moment who is currently in 1st year studying in an undergraduate of Creative Media. He has experience and skills in Graphic Design (3D design & rendering) as well as knowledge in digital marketing.\r\n\r\nIs it possible for someone like him to be a part of the DIG?', 'HTML', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_news`
--

CREATE TABLE IF NOT EXISTS `dig_core_news` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `catagroyid` int(64) NOT NULL,
  `userid` int(64) NOT NULL,
  `created` datetime NOT NULL,
  `subject` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `canreply` varchar(64) DEFAULT 'YES',
  `attachment` int(64) DEFAULT NULL,
  `readercount` int(32) DEFAULT NULL,
  `status` varchar(64) DEFAULT 'VISIABLE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `dig_core_news`
--

INSERT INTO `dig_core_news` (`id`, `catagroyid`, `userid`, `created`, `subject`, `message`, `canreply`, `attachment`, `readercount`, `status`) VALUES
(2, 4, 1, '2016-10-04 02:13:28', 'asdasdas', '<p>\n								asdasdsad<img src="/ueditor/php/upload/image/20161003/1475506580100823.jpg" alt="1475506580100823.jpg"/></p>', NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_news_catagroy`
--

CREATE TABLE IF NOT EXISTS `dig_core_news_catagroy` (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `descriptionformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `timecreated` datetime NOT NULL,
  `timemodified` datetime NOT NULL,
  `usermodified` int(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `dig_core_news_catagroy`
--

INSERT INTO `dig_core_news_catagroy` (`id`, `title`, `description`, `descriptionformat`, `timecreated`, `timemodified`, `usermodified`) VALUES
(3, 'Project', '', 'HTML', '2016-09-30 23:30:34', '2016-09-30 23:30:34', 1),
(4, 'Project', '', 'HTML', '2016-09-30 23:31:29', '2016-09-30 23:31:29', 1),
(5, 'popop', '', 'HTML', '2016-10-01 00:32:57', '2016-10-01 00:32:57', 1),
(6, 'aaa', '', 'HTML', '2016-10-01 00:34:16', '2016-10-01 00:34:16', 1),
(7, 'aaa', '', 'HTML', '2016-10-01 00:34:41', '2016-10-01 00:34:41', 1),
(8, 'ppppp', '', 'HTML', '2016-10-01 00:37:50', '2016-10-01 00:37:50', 1),
(9, 'lll', '', 'HTML', '2016-10-03 17:09:55', '2016-10-03 17:09:55', 1),
(10, 'nm', '', 'HTML', '2016-10-03 17:10:17', '2016-10-03 17:10:17', 1),
(11, 'Project', '', 'HTML', '2016-10-03 17:25:17', '2016-10-03 17:25:17', 1);

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_project`
--

CREATE TABLE IF NOT EXISTS `dig_core_project` (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `descriptionformat` varchar(64) DEFAULT 'HTML',
  `location` varchar(64) DEFAULT NULL,
  `catagroyid` int(64) NOT NULL,
  `ownerid` int(64) NOT NULL,
  `path` text NOT NULL,
  `timecreated` datetime NOT NULL,
  `timemodified` datetime NOT NULL,
  `usermodified` int(64) NOT NULL,
  `status` varchar(64) DEFAULT 'VISIABLE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `dig_core_project`
--

INSERT INTO `dig_core_project` (`id`, `fullname`, `description`, `descriptionformat`, `location`, `catagroyid`, `ownerid`, `path`, `timecreated`, `timemodified`, `usermodified`, `status`) VALUES
(1, 'title', 'overview', NULL, NULL, 1, 1, 'filename', '2016-10-04 01:44:58', '2016-10-04 01:44:58', 1, NULL),
(2, 'asdas', 'asdas', NULL, NULL, 12, 1, 'Assessment Task 1 Rubric.pdf', '2016-10-04 01:45:24', '2016-10-04 01:45:24', 1, NULL),
(5, 'sdaadaaaaaaaaaaa', 'sadddddddddddddddddddddddddddddddddddddd', NULL, NULL, 12, 1, 'Team 7.pdf', '2016-10-04 02:31:12', '2016-10-04 02:31:12', 1, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_project_catagroy`
--

CREATE TABLE IF NOT EXISTS `dig_core_project_catagroy` (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(64) NOT NULL,
  `imgfilename` text NOT NULL,
  `imgpath` text NOT NULL,
  `descriptionformat` varchar(64) NOT NULL DEFAULT 'HTML',
  `timecreated` datetime NOT NULL,
  `timemodified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `dig_core_project_catagroy`
--

INSERT INTO `dig_core_project_catagroy` (`id`, `fullname`, `imgfilename`, `imgpath`, `descriptionformat`, `timecreated`, `timemodified`) VALUES
(12, 'Game Development', 'game.png', 'uploads/9069b86ee967bd31332d0d921273b5ce.png', 'HTML', '2016-10-03 00:00:00', NULL),
(13, 'Multimedia', 'media.png', 'uploads/1927de5d898169f94148e0bf3a52b44b.png', 'HTML', '2016-10-03 00:00:00', NULL),
(14, 'Software Development', 'computer.png', 'uploads/aa57118d436ca0cb21a8605b3609a134.png', 'HTML', '2016-10-03 00:00:00', NULL),
(15, 'App Development', 'app.png', 'uploads/35dd12ae8d38ede9b1343f297d806aba.png', 'HTML', '2016-10-03 00:00:00', NULL),
(16, 'Wearable Computing', 'wearable.png', 'uploads/1f7d1e45ed8e594d7ec36920dc2c97a1.png', 'HTML', '2016-10-03 00:00:00', NULL),
(17, 'Security', 'secrit.png', 'uploads/dfa28fae593e7150fbe6df5d319cbcf2.png', 'HTML', '2016-10-03 00:00:00', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dig_core_project_resource`
--

CREATE TABLE IF NOT EXISTS `dig_core_project_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `belong` varchar(64) NOT NULL DEFAULT 'PROJECT',
  `parentid` int(11) NOT NULL,
  `path` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `size` int(11) NOT NULL,
  `status` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
