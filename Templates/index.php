<?php
/* Set oracle user login and password info */
// $dbuser = "hyou"; /* your deakin login */
// $dbpass = "Yhx91311"; /* your oracle access password */
// $db = "SSID";
// $connect = oci_connect($dbuser, $dbpass, $db);
//
// if (!$connect) {
//   echo "An error occurred connecting to the database";
//   exit;
// }
// $query = "SELECT * FROM (SELECT * FROM dig_news order by id)  where ROWNUM <= 6";
//
// $result = oci_parse($connect, $query);
//
// if(!$result) {
//   echo "An error occurred in parsing the sql string.\n";
//   exit;
// }
// else oci_execute($result);
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Freelancer - Start Bootstrap Theme</title>

  <!-- Bootstrap Core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body id="page-top" class="index">

  <!-- Navigation -->
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="#page-top">Start Bootstrap</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
          <li class="page-scroll">
            <a href="#project">Project</a>
          </li>
          <li class="page-scroll">
            <a href="#news">News</a>
          </li>
          <li class="page-scroll">
            <a href="forum.php">Forum</a>
          </li>
          <li class="page-scroll">
            <a href="reg-page.php">Reg</a>
          </li>
          <li class="page-scroll">
            <a href="login-page.php">Login</a>
          </li>

        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>

  <!-- Header -->
  <header style="background: #a6bc69;">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">

          <div class="intro-text">

            <div class="form-group col-xs-11 floating-label-form-group controls" style="border-left: 0px none;">

              <span><input class="form-control fa fa-search"id="search"></span>

              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group col-xs-1 floating-label-form-group controls" style="border-left: 0px none;">
              <i class="fa fa-search fa-3x" style="font-size:3.15em" aria-hidden="true"></i>
            </div>

          </div>
        </div>
        <div class="col-lg-2">
        </div>
      </div>
    </div>
  </header>

  <!-- Portfolio Grid Section -->
  <section id="portfolio">
    <div class="container">
      <div  class="col-lg-9">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2>News</h2>
            <hr class="primary">
          </div>
        </div>

        <div class="row" style="margin: 0px;">

          <?php
          while(oci_fetch($result)) {
            $title = oci_result($result,2);
            $postdate = oci_result($result,3);
            $detail = oci_result($result,4);
            $author = oci_result($result,5);
            $id = oci_result($result,1);
            if($title != "")
            {
              echo"
              <div class='col-sm-12 portfolio-item' style='background-color:white;'>
              <div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
              <div class='col-sm-12 carboard-news-title'>
              <a href='news-detail.php?id=$id'><h3>$title</h3></a>
              </div>

              <div class='col-sm-12 carboard-news-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
              <p>$detail</p>

              </div>
              <div style='height:5px'> </div>
              <div class='col-sm-12'>
              <a href='news-detail.php?id=$id'>Read More</a>
              <hr class='primary'>
              </div>
              <div class='col-sm-12'>
              <div class='col-sm-6' style='padding-left:0px'>
              <p class='small pull-left'><a href='#'>$author</a></p>
              </div>
              <div class='col-sm-6'>
              <p class='small pull-right'>$postdate</p>
              </div>
              </div>

              </div>
              </div>";
            }

          }
          ?>

        </div>
      </div>

      <div  class="col-lg-3">

        <div class="row">
          <div class="col-lg-12 text-center">
            <h2>Projects</h2>
            <hr class="primary">
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">Game Development</i>
                </div>
              </div>
              <img src="img/portfolio/cabin.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">Multimedia</i>
                </div>
              </div>
              <img src="img/portfolio/cake.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">Software Development</i>
                </div>
              </div>
              <img src="img/portfolio/circus.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">App Development</i>
                </div>
              </div>
              <img src="img/portfolio/game.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">Robotics</i>
                </div>
              </div>
              <img src="img/portfolio/safe.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">Wearable Computing</i>
                </div>
              </div>
              <img src="img/portfolio/submarine.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">Security</i>
                </div>
              </div>
              <img src="img/portfolio/cabin.png" class="img-responsive" alt="">
            </a>
          </div>
          <div class="col-sm-12 portfolio-item">
            <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
              <div class="caption">
                <div class="caption-content">
                  <i class="fa">IT for Health</i>
                </div>
              </div>
              <img src="img/portfolio/cabin.png" class="img-responsive" alt="">
            </a>
          </div>
        </div>


      </div>



    </div>
  </section>




  <!-- Footer -->
  <footer class="text-center">
    <div class="footer-above">
      <div class="container">
        <div class="row">
          <div class="footer-col col-md-4">
            <h3>Location</h3>
            <p>3481 Melrose Place
              <br>Beverly Hills, CA 90210</p>
            </div>
            <div class="footer-col col-md-4">
              <h3>Around the Web</h3>
              <ul class="list-inline">
                <li>
                  <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                </li>
                <li>
                  <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                </li>
                <li>
                  <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                </li>
                <li>
                  <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                </li>
                <li>
                  <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                </li>
              </ul>
            </div>
            <div class="footer-col col-md-4">
              <img src="img/portfolio/logo.png" class="img-responsive img-centered" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="footer-below">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              Copyright &copy; Your Website 2016
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
      <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>



    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>

  </body>

  </html>
