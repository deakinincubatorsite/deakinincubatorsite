﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DIG: Opportunities</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/freelancer.min.css" rel="stylesheet">
	
	<!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
 <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="index.php">DIG</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="project.php">Project</a>
                    </li>
                    <li class="page-scroll">
                        <a href="news.php">News</a>
                    </li>
					<li class="page-scroll active">
                        <a href="#">About</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

 <section>
     <!-- Page Content -->
    <div class="container" style="margin-top: 20px;">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>Deakin lecturers hatch IT incubator</h1>

                <!-- Author -->
                <p class="lead">
                  
                </p>

                <div class="col-lg-12">
				 <a href="#">Shaun</a>
				   <p class="pull-right" style="font-size: 14px;"s><span class="glyphicon glyphicon-time"></span> Posted 14 June 2015</p>
				    <hr>
				</div>

                <!-- Date/Time -->
                

                <hr>

                

                <!-- Post Content -->
                <div>
								<p class="small">
					<strong>The Deakin Incubator Group is set to give students &#39;real world&#39; IT experience.</strong><strong><img class="pull-right" alt="" title="" src="http://www.deakin.edu.au/__data/assets/image/0007/527344/1434351475-greg-sophie-instory.jpg" height="382" width="300"/></strong>
				 &nbsp;&nbsp; <br/>
				</p>
				<p class="small">
						Facebook, Google and Microsoft were all established while their founders were still at university.
				</p>
				<p class="small">
					
					A small group of academics from Deakin’s School of Information 
				Technology are about to give their students a chance to set up 
				commercial companies and gain a competitive edge when they enter the 
				market place.
				</p>
				<p class="small">
						Lecturers Dr Greg Bowtell, Dr Shaun Bangay and Mrs 
				Sophie McKenzie, with support from Dr Michael Hobbs, Dr Tim Wilkin and 
				Ms Melanie Randall, have set up the Deakin Incubator Group (DIG) that 
				will be open to students from all disciplines at Deakin, including IT, 
				marketing, business, communications and creative arts – and give them 
				“real world” experience before they leave the campus.
				</p>
				<p class="small">
						Dr Bowtell 
				was among the first intake of Deakin’s Bachelor of Game Design in 2005. 
				After spending some time in the workforce, he returned to complete his 
				honours and PhD study, focussing on identifying ways that the curriculum
				 could continue to produce job-ready graduates in a competitive market.
				</p>
				<p class="small">
					
					“My research showed that IT students and others, particularly those in 
				the creative industries, need experience on commercial projects, to 
				improve their job-readiness, before they graduate,” he said.
				</p>
				<p class="small">
						“The
				 new hub will provide evidence that students have the commitment and 
				passion to complete extra-curricular work – and show they have the 
				wherewithal to gain the skills they need.”
				</p>
				<p class="small">
						The hub will be open 
				to any enrolled student at Deakin and projects could span the spectrum 
				of IT-related jobs, from app design, to augmented reality, game design, 
				or wearable computing. Free apps created by the students - showcasing 
				their talents - will be available from the DIG website.
				</p>
				<p class="small">
						Using 
				Deakin’s Cloud learning environment, which supports on-line learning and
				 distance education for over 10,000 students, the DIG students will work
				 in teams, collaborating, designing and producing innovative solutions 
				for external clients and researchers at Deakin. They will be actively 
				seeking interest from businesses, to discuss potential projects.
				</p>
				<p class="small">
						Dr Bowtell explained that students will apply to join DIG’s talent pool,
				 as they would with any formal freelance agency. They can then apply for
				 specific jobs as they arise, bringing their technical, design, 
				marketing or programming skills to the task.
				</p>
				<p class="small">
						“Freelancing is common in IT,” he said. “Each job will have different demands and time commitments, just as in the workforce.”
				</p>
				<p class="small">
					
					One of the first projects to be completed was an anti-bullying computer
				 game, called “Safekeeper,” which was developed alongside post-graduate 
				students from Deakin’s Education Faculty and featured input from a small
				 group of high school students. Instead of engaging in combat, like most
				 popular computer games, players of “Safekeeper” work together, 
				developing skills in teamwork and conflict resolution, and learning the 
				benefits of altruism.
				</p>
				<p class="small">
						Dr Bowtell noted that an important goal of 
				DIG will be to encourage the formation of companies that produce and 
				license content.
				</p>
				<p class="small">
						“DIG will not only provide a structured 
				‘simulated studio’ environment, but will also produce commercial 
				applications and seed future game studios.&nbsp; Deakin will, in effect, 
				house a number of functioning development teams within a large studio,” 
				he said.
				</p>
				<p class="small">
						An equally attractive drawcard is the potential for 
				financial reward. Students involved in a project will receive a 
				financial share if their project is successful. Who knows, the next 
				Facebook or Google could come from Victoria, Australia.
				</p>
				<p class="small">
					&nbsp; &nbsp;	<strong>For more information, contact Dr Greg Bowtell at </strong><a style="color: rgb(118, 146, 60); text-decoration: underline;" href="mailto:greg.bowtell@deakin.edu.au"><span style="color: rgb(118, 146, 60);"><strong>greg.bowtell@deakin.edu.au</strong></span></a><strong>/ tel: 03 5227 2960 or Dr Shaun Bangay at </strong><a style="color: rgb(118, 146, 60); text-decoration: underline;" href="mailto:shaun.bangay@deakin.edu.au"><span style="color: rgb(118, 146, 60);"><strong>shaun.bangay@deakin.edu.au</strong></span></a>
				</p>
				<p class="small">
					<br/>
				</p>
				
				</div>
               
                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="">
                    <h4>Leave a Comment:</h4>
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control" style="border-radius: 0px;"  rows="3"></textarea>
                        </div>
                        <button type="submit" style="border-radius: 0px;" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->
                

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                    <h4>News Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control" style="border-radius: 0px;">
                        <span class="input-group-btn" style="border-radius: 0px;">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                    <h4>New Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category 1</a>
                                </li>
                                <li><a href="#">Category 2</a>
                                </li>
                                <li><a href="#">Category 3</a>
                                </li>
                                <li><a href="#">Category 4</a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                    <!-- /.row -->
                </div>
				
				 <!-- Blog Categories Well -->
                <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                    <h4>Project Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">APP</a>
                                </li>
                                <li><a href="#">Category 2</a>
                                </li>
                                <li><a href="#">Category 3</a>
                                </li>
                                <li><a href="#">Category 4</a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                    <!-- /.row -->
                </div>

               
            </div>

        </div>
        <!-- /.row -->

        <hr>

    </div>
 </section>
 
  <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>3481 Melrose Place
                            <br>Beverly Hills, CA 90210</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <img class='img-responsive' src="img/portfolio/logo.png" class="img-responsive img-centered" alt="">
                    </div>
                </div>
            </div>
        </div>
       
    </footer>
     <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>

</body>

</html>
