<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Dashboard</title>
	
	<!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	
    <link href="../css/bootstrap-wysiwyg.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin Dashboard</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="admin-dashboard-page.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="project-manage.html"><i class="fa fa-fw fa-tasks"></i> Project Manage</a>
                    </li>
                    <li  class="active">
                        <a href="article-manage.html"><i class="fa fa-fw fa-heart"></i> Article Manage</a>
                    </li>
                    <li>
                        <a href="user-manage.html"><i class="fa fa-fw fa-user"></i> User Manage</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Article Manage
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i> Article Manage
                            </li>


                            
                                <div class="btn-group" style="float:right">
                                  <a href="article-create.html"><button type="button"  role="button" class="btn btn-success btn-xs">Create new Article</button></a>

                                </div>
                            


                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                <div class="col-lg-12">
                <table class="table table-striped">
                    <tr>
                      <td>Artice Title</td>
                      <td>Author</td>
                      <td>Create Time</td>
                      <td>Status</td>
                      <td>Action</td>
                    </tr> 
                    <tr>
                      <td>Artilce title sssssssssssssssssssssss</td>
                      <td>John</td>
                      <td>09-09-2016</td>
                      <td>Stopped</td>
                      <td>
                        <a href="#" class="btn btn-success btn-xs" role="button"><i class="fa fa-fw fa-play"></i></a>
                        <a href="article-modify.html" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
                        <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Article 1" role="button"><i class="fa fa-fw fa-trash"></i></a>
                        
                      </td>
                      
                    </tr>
                    <tr>
                      <td>Artilce title</td>
                      <td>John</td>
                      <td>11-09-2016</td>
                      <td>Actived</td>  
                      <td>
                        <a href="#" class="btn btn-danger btn-xs" role="button"><i class="fa fa-fw fa-stop"></i></a>
                        <a href="article-modify.html" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
                        <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Article 3" role="button"><i class="fa fa-fw fa-trash"></i></a>
                        
                      </td>
                      
                    </tr>
                    <tr>
                      <td>Artilce title</td>
                      <td>John</td>
                      <td>07-09-2016</td>
                      <td>Actived</td>  
                      <td>
                        <a href="#" class="btn btn-danger btn-xs" role="button"><i class="fa fa-fw fa-stop"></i></a>
                        <a href="article-modify.html" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
                        <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Article 4" role="button"><i class="fa fa-fw fa-trash"></i></a>
                        
                      </td>
                      
                    </tr>      
                    <tr>
                      <td>Artilce title</td>
                      <td>John</td>
                      <td>09-09-2016</td>
                      <td>Actived</td>  
                      <td>
                        <a href="#" class="btn btn-danger btn-xs" role="button"><i class="fa fa-fw fa-stop"></i></a>
                        <a href="article-modify.html" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
                        <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Article 5" role="button"><i class="fa fa-fw fa-trash"></i></a>
                        
                      </td>
                      
                    </tr>      
                    <tr>
                      <td>Artilce title</td>
                      <td>John</td>
                      <td>09-09-2016</td>
                      <td>Actived</td>  
                      <td>
                        <a href="#" class="btn btn-danger btn-xs" role="button"><i class="fa fa-fw fa-stop"></i></a>
                        <a href="article-modify.html" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
                        <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Article 9" role="button"><i class="fa fa-fw fa-trash"></i></a>
                        
                      </td>
                      
                    </tr>      
                    <tr>
                      <td>Artilce title 2</td>
                      <td>John</td>
                      <td>27-08-2016</td>
                      <td>Actived</td>  
                      <td>
                        <a href="#" class="btn btn-danger btn-xs" role="button"><i class="fa fa-fw fa-stop"></i></a>
                        <a href="article-modify.html" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
                        <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Article 11" role="button"><i class="fa fa-fw fa-trash"></i></a>
                        
                      </td>
                      
                    </tr>                      
                </table>

                </div>
                

                </div>

               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- /#Message BOx -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content panel-danger">
			  <div class="modal-header panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Confirm to delete </h4>
			  </div>
			 
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Confirm</button>
			  </div>
			</div>
		  </div>
		</div>
		<!-- /#Message BOx -->
     <!-- jQuery -->
    <script src="../js/jquery.js"></script>

	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
    <script src="../js/plugins/flot/jquery.flot.js"></script>
    <script src="../js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="../js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="../js/plugins/flot/flot-data.js"></script>
	
	<script>
	
	$('#editor').wysiwyg();
	
		$('#exampleModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Confirm to delete article (' + recipient + ')')
	  modal.find('.modal-body input').val(recipient)
	})
	</script>
	

</body>

</html>
