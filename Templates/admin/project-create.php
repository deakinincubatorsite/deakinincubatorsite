<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Dashboard</title>
	
    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	
    <link href="../css/bootstrap-wysiwyg.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin Dashboard</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="admin-dashboard-page.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">
                        <a href="project-manage.html"><i class="fa fa-fw fa-tasks"></i> Project Manage</a>
                    </li>
                    <li>
                        <a href="article-manage.html"><i class="fa fa-fw fa-heart"></i> Article Manage</a>
                    </li>
                    <li>
                        <a href="user-manage.html"><i class="fa fa-fw fa-user"></i> User Manage</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Create a Project
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i>  <a href="article-manage.html">Project Manage</a>
                            </li>
							 <li class="active">
                                <i class="fa fa-tasks"></i> New Project
                            </li>

                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
					<div class="col-lg-12">
						<form name="news" method="POST" action="php/projectpush.php"> 
						  <div class="form-group">
							<label for="articleTitel">Title</label>
							<input type="text" class="form-control" name="title" id="articleTitel"  placeholder="Enter title">
						  </div>
						
						   <div class="form-group">
							<label for="mentors">Key Value 1</label>
							<input type="text" class="form-control" name="key1" id="mentors"  placeholder="">
						  </div>
						  
						  <div class="form-group">
							<label for="Client">Key Value 2</label>
							<input type="text" class="form-control" name="key2" id="Client"  placeholder="">
						  </div>
						  
						  <div class="form-group">
							<label for="contacts">Project Contacts</label>
							 <textarea class="form-control" name="contacts" id="contacts" rows="3"></textarea>
						  </div>
						  
						  <div class="form-group">
							<label for="contacts">Overview</label>
							 <textarea class="form-control" name="overview" id="contacts" rows="3"></textarea>
						  </div>
						  
						  <div class="form-group">
							<label for="exampleSelect2">Category </label>
							<select multiple class="form-control" name="category" id="exampleSelect2">
							  <option>Game Development</option>
							  <option>Multimedia</option>
							  <option>Software Development</option>
							  <option>App Development</option>
							  <option>Robotics</option>
							  <option>Wearable Computing</option>
							  <option>Security</option>
							  <option>IT for Health</option>
							</select>
						  </div>
						  <div class="form-group">
							<label for="Client">PDF</label>
							<input type="text" class="form-control" name ="pdf" id="Client"  placeholder="">
						  </div>
							
						  <button type="submit" class="btn btn-danger" style="width: 150px;">Back</button>
							<button type="submit" class="btn btn-primary pull-right" style="width: 150px;">Publish</button>
						</form>

					</div>
                

                </div>

               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
   <!-- jQuery -->
    <script src="../js/jquery.js"></script>

	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
    <script src="../js/plugins/flot/jquery.flot.js"></script>
    <script src="../js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="../js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="../js/plugins/flot/flot-data.js"></script>
	
	<script type="text/javascript" src="../js/plugins/editor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/plugins/editor/lang/en/en.js"></script>
	<script type="text/javascript" src="../js/plugins/editor/ueditor.all.js"></script>
    <!-- ?????? -->
    <script type="text/javascript">
        var ue = UE.getEditor('project-contact');
		var ue = UE.getEditor('project-container');
    </script>

</body>

</html>
