<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

     <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">Start Bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#project">Project</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#news">News</a>
                    </li>
					<li class="page-scroll">
                        <a href="#news">Forum</a>
                    </li>
					<li class="page-scroll">
                        <a href="reg-page.php">Reg</a>
                    </li>
					<li class="page-scroll">
                        <a href="login-page.php">Login</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        
        <hr>

        <!-- Title -->
		<br/>
		<br/>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-6 form-loginpage">
            <h1>Log In</h1>
			<hr>
			<form role="form">
			  <div class="form-group">
				<label for="exampleInputEmail1">E-mail Address
				</label>
				<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email address" required="required">
				
			  </div>
			  <div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Enter Password" required="required">
			  </div>
			  
			  <span><a href="#"><button type="button" class="btn btn-link" data-toggle="modal" data-target="#forgotpassword" role="button">Forgot Password?</button></a></span>
			  
			  
			  

			 <br/>

			  <button type="submit" class="btn btn-success btn-block">LOGIN</button>
			 
			</form>
			
			<span><a href="user-dashboard-page.html"><button type="button" class="btn btn-link" >View user dashboard(functioning prototype)</button></a></span>
			  <span><a href="admin-dashboard-page.html"><button type="button" class="btn btn-link">View admin dashboard(functioning prototype)</button></a></span>
			
		</div>
		<div class="col-lg-3">
		</div>


        <!-- /.row -->

        <!-- Page Features -->
 
        <!-- /.row -->


       

    </div>
    <!-- /.container -->
	
	
	<!-- /#Message BOx -->
		<div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content panel-success">
				<div class="modal-header panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Forgot Password </h4>
				</div>
				<div class="modal-body">
				  <form role="form">
				  <div class="form-group">
					<label for="exampleInputEmail1">Enter the email address you used to sign up and we'll send you a link to reset your password.</label>
					<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter e-mail address">
				  </div>
				<br/>
				</div>
				<div class="modal-footer">
					<br/>
					<button type="button" class="btn btn-default" data-dismiss="modal">Back to login</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Back to login</button>
					<button type="button" class="btn btn-primary">Continue</button>
				</div>
				</form>
			</div>
		  </div>
		</div>
		<!-- /#Message BOx -->
		
		

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>
	
	
</body>

</html><script type="text/javascript">
	var g_duration = 93;
var g_iisLatency = 1;
var g_requireJSDone = new Date().getTime();
</script>