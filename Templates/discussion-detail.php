<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Freelancer - Start Bootstrap Theme</title>

  <!-- Bootstrap Core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body id="page-top" class="index">

  <!-- Navigation -->
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="#page-top">Start Bootstrap</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
          <li class="page-scroll">
            <a href="#project">Project</a>
          </li>
          <li class="page-scroll">
            <a href="#news">News</a>
          </li>
          <li class="page-scroll">
            <a href="">Forum</a>
          </li>

        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>

  <!-- Header -->
  <header style="background: #a6bc69;">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">

          <div class="intro-text">

            <div class="form-group col-xs-11 floating-label-form-group controls" style="border-left: 0px none;">

              <span><input class="form-control fa fa-search"id="search"></span>

              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group col-xs-1 floating-label-form-group controls" style="border-left: 0px none;">
              <i class="fa fa-search fa-3x" style="font-size:3.15em" aria-hidden="true"></i>
            </div>

          </div>
        </div>
        <div class="col-lg-2">
        </div>
      </div>
    </div>
  </header>

  <!-- Portfolio Grid Section -->
  <section id="portfolio">
    <div class="container">
      <div  class="col-lg-3">

        <div class="row">
          <div class="col-lg-12 text-center">
            <h2>Forum</h2>
            <hr class="primary">
          </div>
        </div>
        <div class="row">
          <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
            <h4>Discussion Search</h4>
            <div class="input-group">
              <input type="text" class="form-control" style="border-radius: 0px;">
              <span class="input-group-btn" style="border-radius: 0px;">
                <button class="btn btn-default" type="button">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
            <!-- /.input-group -->
          </div>

          <!-- Blog Categories Well -->
          <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
            <h4>Discussion Categories</h4>
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled">
                  <li><img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#"> Category 1</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 2</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 3</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 4</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 5</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 6</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 7</a>
                  </li>
                  <img src="https://d2l.deakin.edu.au/d2l/img/0/D2L_LE_Core.ActivityTypes.infDiscussion24.png?v=10.6.0.2348-197"/> <a href="#">Category 8</a>
                  </li>

                </ul>
              </div>

            </div>
            <!-- /.row -->
          </div>

          <!-- Blog Categories Well -->

        </div>


      </div>
      <div  class="col-lg-9">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2>discussion</h2>
            <hr class="primary">
          </div>
        </div>

        <div class="row" style="margin: 0px;">
          <div class='col-sm-12 portfolio-item' style='background-color:white;'>
            <div class='col-sm-12' style='' >

              <div class='col-sm-12 portfolio-item' style='background-color:white;'>
                <div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
                  <div class='col-sm-12 carboard-news-title'>
                    <a href='topic-detail.php?id=$id'><h4>DIG T3 Meeting- 30/9/15</h4></a>
                  </div>

                  <div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
                    <h5>CHRIS WARD posted 28 September, 2015 9:09 AM</h5>
                    <p>Hi All,
                      <br/>
                      <br/>I would love to have gone to or Skyped into the meeting this Wednesday, however as I am working full time as I study Cloud, I unfortunately wont be finished work in time for this meeting.
                      <br/>
                      <br/>Will this meeting be recorded for me to look over in my own time?
                      <br/>
                      <br/>Kind regards,

                      Chris.</P>
                    </div>
                    <div style='height:5px'> </div>
                    <div class='col-sm-12'>
                      <a href='topic.php'>Read More</a>
                      <hr class='primary'>
                    </div>
                    <div class='col-sm-12'>
                      <div class='col-sm-8'>
                        <div class='col-sm-6' style='padding-left:0px; margin-top: 21px;"'>
                          <div class="col-sm-3">
                            <div class="d2l-textblock d2l-textblock-strong d2l_1_180_770">3</div>
                            <a class="vui-link d2l-link vui-link-main d2l-link-main d2l_1_181_417" href="javascript:void(0);" onclick="D2L.O(&quot;__g1&quot;,18)();return false;" title="View unread replies for DIG T3 Meeting- 30/9/15">Unread</a>
                          </div>
                          <div class="col-sm-3">
                            <div class="d2l-textblock d2l_1_180_770">3</div>
                            <div class="d2l-textblock d2l_1_181_417"/>Replies</div>
                          </div>
                          <div class="col-sm-1">
                            <div class="d2l-textblock d2l_1_180_770">3</div>
                            <div class="d2l-textblock d2l_1_181_417">Viewer</div>
                          </div>
                        </div>
                      </div>
                      <div class='col-sm-1'>
                        <image style="width: 50px;" src ="https://d2l.deakin.edu.au/d2l/common/viewprofileimage.d2l?oi=6605&ui=134644&s=100&lm=635597530511230000&v=11&cui=114172&v=10.6.0.2348-197&v=10.6.0.2348-197"/>
                      </div>
                      <div class='col-sm-3'>
                        <p>Last post 12 October, 2015 7:38 PM by</P>
                          <p class='small'>Greg Bowtell</p>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class='col-sm-12 portfolio-item' style='background-color:white;'>
                    <div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
                      <div class='col-sm-12 carboard-news-title'>
                        <a href='topic-detail.php?id=$id'><h4>Can someone I know from outside of Deakin join the DIG?</h4></a>
                      </div>

                      <div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
                        <h5>WASSIM GULAM ALI MOKHTAR posted 09 April, 2015 2:21 PM</h5>
                        <p>I have a friend who is studying in a university in Hong Kong at the moment who is currently in 1st year studying in an undergraduate of Creative Media. He has experience and skills in Graphic Design (3D design & rendering) as well as knowledge in digital marketing.
                          <br/>
                          <br/>Is it possible for someone like him to be a part of the DIG?</P>
                        </div>
                        <div style='height:5px'> </div>
                        <div class='col-sm-12'>
                          <a href='topic.php'>Read More</a>
                          <hr class='primary'>
                        </div>
                        <div class='col-sm-12'>
                          <div class='col-sm-8'>
                            <div class='col-sm-6' style='padding-left:0px; margin-top: 21px;"'>
                              <div class="col-sm-3">
                                <div class="d2l-textblock d2l-textblock-strong d2l_1_180_770">3</div>
                                <a class="vui-link d2l-link vui-link-main d2l-link-main d2l_1_181_417" href="javascript:void(0);" onclick="D2L.O(&quot;__g1&quot;,18)();return false;" title="View unread replies for DIG T3 Meeting- 30/9/15">Unread</a>
                              </div>
                              <div class="col-sm-3">
                                <div class="d2l-textblock d2l_1_180_770">2</div>
                                <div class="d2l-textblock d2l_1_181_417">Replies</div>
                              </div>
                              <div class="col-sm-1">
                                <div class="d2l-textblock d2l_1_180_770">10</div>
                                <div class="d2l-textblock d2l_1_181_417">Viewer</div>
                              </div>
                            </div>
                          </div>
                          <div class='col-sm-1'>
                            <image style="width: 50px;" src ="https://d2l.deakin.edu.au/d2l/common/viewprofileimage.d2l?oi=6605&ui=69972&s=100&lm=634656514942600000&v=11&cui=114172&v=10.6.0.2348-197&v=10.6.0.2348-197"/>
                          </div>
                          <div class='col-sm-3'>
                            <p>Last post 12 October, 2015 7:38 PM by</P>
                              <p class='small'>Greg Bowtell</p>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class='col-sm-12 portfolio-item' style='background-color:white;'>
                        <div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
                          <div class='col-sm-12 carboard-news-title'>
                            <a href='topic-detail.php?id=$id'><h4>Spark session on ka3.406 at 5pm. 4th May</h4></a>
                          </div>

                          <div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
                            <h5>MELISSA DEGALA posted 24 April, 2015 10:05 AM</h5>
                            <p>Hi DIG,
                              <br/>
                              <br/>I was wondering where I can register to attend this event.  I am an off-campus Deakin student, studying the BA of IT in mobile apps development.
                              <br/>
                              <br/>Thank you,
                              <br/>
                              <br/>Melissa
                              <br/>
                              <br/>Chris.</P>
                            </div>
                            <div style='height:5px'> </div>
                            <div class='col-sm-12'>
                              <a href='topic.php'>Read More</a>
                              <hr class='primary'>
                            </div>
                            <div class='col-sm-12'>
                              <div class='col-sm-8'>
                                <div class='col-sm-6' style='padding-left:0px; margin-top: 21px;'>
                                  <div class="col-sm-3">
                                    <div class="d2l-textblock d2l-textblock-strong d2l_1_180_770">3</div>
                                    <a class="vui-link d2l-link vui-link-main d2l-link-main d2l_1_181_417" href="javascript:void(0);" onclick="D2L.O(&quot;__g1&quot;,18)();return false;" title="View unread replies for DIG T3 Meeting- 30/9/15">Unread</a>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="d2l-textblock d2l_1_180_770">1</div>
                                    <div class="d2l-textblock d2l_1_181_417">Replies</div>
                                  </div>
                                  <div class="col-sm-1">
                                    <div class="d2l-textblock d2l_1_180_770">17</div>
                                    <div class="d2l-textblock d2l_1_181_417">Viewer</div>
                                  </div>
                                </div>
                              </div>
                              <div class='col-sm-1'>
                                <image style="width: 50px;" src ="https://d2l.deakin.edu.au/d2l/common/viewprofileimage.d2l?oi=6605&ui=134644&s=100&lm=635597530511230000&v=11&cui=114172&v=10.6.0.2348-197&v=10.6.0.2348-197"/>
                              </div>
                              <div class='col-sm-3'>
                                <p>Last post 29 April, 2015 11:30 AM by</P>
                                  <p class='small'>Greg Bowtell</p>
                                </div>
                              </div>

                            </div>
                          </div>

                        </div>
                      </section>




                      <!-- Footer -->
                      <footer class="text-center">
                        <div class="footer-above">
                          <div class="container">
                            <div class="row">
                              <div class="footer-col col-md-4">
                                <h3>Location</h3>
                                <p>3481 Melrose Place
                                  <br>Beverly Hills, CA 90210</p>
                                </div>
                                <div class="footer-col col-md-4">
                                  <h3>Around the Web</h3>
                                  <ul class="list-inline">
                                    <li>
                                      <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                                    </li>
                                    <li>
                                      <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                                    </li>
                                    <li>
                                      <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                                    </li>
                                    <li>
                                      <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                                    </li>
                                    <li>
                                      <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                                    </li>
                                  </ul>
                                </div>
                                <div class="footer-col col-md-4">
                                  <img src="img/portfolio/logo.png" class="img-responsive img-centered" alt="">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="footer-below">
                            <div class="container">
                              <div class="row">
                                <div class="col-lg-12">
                                  Copyright &copy; Your Website 2016
                                </div>
                              </div>
                            </div>
                          </div>
                        </footer>

                        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
                        <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
                          <a class="btn btn-primary" href="#page-top">
                            <i class="fa fa-chevron-up"></i>
                          </a>
                        </div>



                        <!-- jQuery -->
                        <script src="vendor/jquery/jquery.min.js"></script>

                        <!-- Bootstrap Core JavaScript -->
                        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

                        <!-- Plugin JavaScript -->
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

                        <!-- Contact Form JavaScript -->
                        <script src="js/jqBootstrapValidation.js"></script>
                        <script src="js/contact_me.js"></script>

                        <!-- Theme JavaScript -->
                        <script src="js/freelancer.min.js"></script>

                      </body>

                      </html>
