<!DOCTYPE html>
<html lang="en">

<head>

    <!-- DataTables CSS -->
    <link href="../../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">News Category</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-radius: 0px;" >
                        <div class="panel-heading" style="padding: 0px;">
							<a href=""class="btn btn-primary"  data-toggle="modal"  data-target="#categoryAddModal" style="border-radius: 0px; margin: 0px; background-color: rgb(166, 188, 105); border-color: aliceblue; width:100%" type="button" role="button">Add a category</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>News Count</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>App Development</td>
                                        <td>13</td>
                                        <td class="center">
											<a href="#" class="btn btn-danger btn-xs" role="button">
											<i class="fa fa-fw fa-stop"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal"  data-target="#categoryUpdateModal" data-whatever="App Development" data-whateverid="1"  role="button"><i class="fa fa-fw fa-cog"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Usersss" role="button"><i class="fa fa-fw fa-trash"></i></a>
										</td>
                                    </tr>
									<tr class="odd gradeX">
                                        <td>Software</td>
                                        <td>10</td>
                                        <td class="center">
											<a href="#" class="btn btn-danger btn-xs" role="button">
											<i class="fa fa-fw fa-stop"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal"  data-target="#categoryUpdateModal" data-whatever="Game Development" data-whateverid="1"  role="button"><i class="fa fa-fw fa-cog"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Usexxx" role="button"><i class="fa fa-fw fa-trash"></i></a>
										</td>
                                    </tr>
									<tr class="odd gradeX">
                                        <td>News</td>
                                        <td>12</td>
                                        <td class="center">
											<a href="#" class="btn btn-success btn-xs" role="button">
											<i class="fa fa-fw fa-play"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal"  data-target="#categoryUpdateModal" data-whatever="Software Development" data-whateverid="1"  role="button"><i class="fa fa-fw fa-cog"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Usexxx" role="button"><i class="fa fa-fw fa-trash"></i></a>
										</td>
                                    </tr>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


	<!-- /#Message BOx -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content panel-danger">
		  <div class="modal-header panel-heading">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Confirm to delete user</h4>
		  </div>

		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Confirm</button>
		  </div>
		</div>
	  </div>
	</div>
	<!-- /#Message BOx -->


	<!-- /#Category Update BOx -->
	<div class="modal fade" id="categoryUpdateModal" tabindex="-1" role="dialog" aria-labelledby="categoryUpdateModal">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Category</h4>
		  </div>
		  <div class="modal-body">
			<form>
			  <div class="form-group">
				<label for="recipient-name" class="control-label">Category:</label>
				<input type="text" class="form-control" id="recipient-name">
			  </div>
			  <div class="id"><input type="text" class="form-control" style="display:none" id="recipient-id"></div>
			</form>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Save</button>
		  </div>
		</div>
	  </div>
	</div>
	<!-- /#Category Update BOx -->


	<!-- /#Category Update BOx -->
	<div class="modal fade" id="categoryAddModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Add a Category</h4>
		  </div>
		  <div class="modal-body">
			<form>
			  <div class="form-group">
				<label for="recipient-name" class="control-label">Category Name:</label>
				<input type="text" class="form-control" id="recipient-name">
			  </div>
			  <div class="id"><input type="text" class="form-control" style="display:none" id="recipient-id"></div>
			</form>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Save</button>
		  </div>
		</div>
	  </div>
	</div>
	<!-- /#Category Update BOx -->



    <!-- jQuery -->
    <script src="../../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../vendor/metisMenu/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
    <script src="../../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../js/sb-admin-2.js"></script>

	 <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

	$('#exampleModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Confirm to delete (' + recipient + ')')
	  modal.find('.modal-body input').val(recipient)
	})


	$('#categoryUpdateModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var recipient = button.data('whatever') // Extract info from data-* attributes
	  var recipientid = button.data('whateverid') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Update category ' + recipient)
	  modal.find('.modal-body input').val(recipient)
	  modal.find('.id input').val(recipientid)
	})


    </script>


</body>

</html>
