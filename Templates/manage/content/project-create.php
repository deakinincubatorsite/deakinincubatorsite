<!DOCTYPE html>
<html lang="en">

<head>

</head>

<body>

    <div id="wrapper">

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Create a Project
                        </h1>
                        <ol class="breadcrumb">
                           <li>
                                <i class="fa fa-dashboard"></i>  <a href="../manage/index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i>  <a href="../manage/index.php?id=4">Project Manage</a>
                            </li>
							 <li class="active">
                                <i class="fa fa-tasks"></i> New Project
                            </li>

                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
					<div class="col-lg-12">
						<form name="news" method="POST" action="php/projectpush.php"> 
						  <div class="form-group">
							<label for="articleTitel">Title</label>
							<input type="text" class="form-control" name="title" id="articleTitel"  placeholder="Enter title">
						  </div>
						
						   <div class="form-group">
							<label for="mentors">Key Value 1</label>
							<input type="text" class="form-control" name="key1" id="mentors"  placeholder="">
						  </div>
						  
						  <div class="form-group">
							<label for="Client">Key Value 2</label>
							<input type="text" class="form-control" name="key2" id="Client"  placeholder="">
						  </div>
						  
						  <div class="form-group">
							<label for="contacts">Project Contacts</label>
							 <textarea class="form-control" name="contacts" id="contacts" rows="3"></textarea>
						  </div>
						  
						  <div class="form-group">
							<label for="contacts">Overview</label>
							 <textarea class="form-control" name="overview" id="contacts" rows="3"></textarea>
						  </div>
						  
						  <div class="form-group">
							<label for="exampleSelect2">Category </label>
							<select multiple class="form-control" name="category" id="exampleSelect2">
							  <option>Game Development</option>
							  <option>Multimedia</option>
							  <option>Software Development</option>
							  <option>App Development</option>
							  <option>Robotics</option>
							  <option>Wearable Computing</option>
							  <option>Security</option>
							  <option>IT for Health</option>
							</select>
						  </div>
						  <div class="form-group">
							<label for="Client">PDF</label>
							<input type="text" class="form-control" name ="pdf" id="Client"  placeholder="">
						  </div>
							
						  <button type="submit" class="btn btn-danger" style="width: 150px;">Back</button>
							<button type="submit" class="btn btn-primary pull-right" style="width: 150px;">Publish</button>
						</form>

					</div>
                

                </div>

               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
   <!-- jQuery -->
    <script src="../js/jquery.js"></script>

	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

  

   
	<script type="text/javascript" src="../js/plugins/editor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/plugins/editor/lang/en/en.js"></script>
	<script type="text/javascript" src="../js/plugins/editor/ueditor.all.js"></script>
    <!-- ?????? -->
    <script type="text/javascript">
        var ue = UE.getEditor('project-contact');
		var ue = UE.getEditor('project-container');
    </script>

</body>

</html>
