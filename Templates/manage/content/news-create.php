﻿<!DOCTYPE html>
<html lang="en">

<head>

</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper" >

            <div class="container-fluid" >

                <!-- Page Heading -->
                <div class="row"  style="margin-bottom:20px">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Publish News
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../manage/index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i>  <a href="../manage/index.php?id=2">News Manage</a>
                            </li>
							 <li class="active">
                                <i class="fa fa-tasks"></i> Create news
                            </li>

                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row" style="margin-bottom: 25px;">
					<div class="col-lg-12">
						<form name="news" method="POST" action="php/newpush.php"> 
						  <div class="form-group">
							<label for="articleTitel">Title</label>
							<input type="text" class="form-control" name="title" id="articleTitel"  placeholder="Enter article title">
							
						  </div>
						  
						 
						  
						  <div class="form-group">
							<label for="articleTitel">Author</label>
							<input type="text" class="form-control" name="author"id="articleTitel"  placeholder="Enter article title">
							
						  </div>
						  
						   <div class="form-group">
							<label for="articleTitel">Postdate</label>
							<input type="text" class="form-control" name="postdate" id="articleTitel"  placeholder="Enter article title">
						  </div>
						 
						 
						  <div class="form-group">
							<label for="exampleTextarea">Content</label>
							<script id="container" name="detail" type="text/plain"  style="width:100%">
								Input your Content......
							</script>
						  </div>
							
						  <button type="submit" class="btn btn-danger" style="width: 150px;">Back</button>
							<button type="submit" class="btn btn-primary pull-right" style="width: 150px;">Publish</button>
						</form>

					</div>
                

                </div>

               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- /#Message BOx -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content panel-danger">
			  <div class="modal-header panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Confirm to delete </h4>
			  </div>
			 
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Confirm</button>
			  </div>
			</div>
		  </div>
		</div>
		<!-- /#Message BOx -->
    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

  
	
	<script type="text/javascript" src="../js/plugins/editor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/plugins/editor/lang/en/en.js"></script>
	<script type="text/javascript" src="../js/plugins/editor/ueditor.all.js"></script>
    <!-- 实例化编辑器 -->
    <script type="text/javascript">
        var ue = UE.getEditor('container');
    </script>

</body>

</html>
