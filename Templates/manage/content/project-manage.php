<!DOCTYPE html>
<html lang="en">

<head>


    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Project Manage</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                     <div class="panel panel-default" style="border-radius: 0px;" >
                        <div class="panel-heading" style="padding: 0px;">
							<a href="index.php?id=41"class="btn btn-primary" style="border-radius: 0px; margin: 0px; background-color: rgb(166, 188, 105); border-color: aliceblue;" type="button" role="button">Create a project</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Owner</th>
                                        <th>Create Time</th>
                                        <th>Expire Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>Trident</td>
                                        <td>Internet Explorer 4.0</td>
                                        <td>Win 95+</td>
                                        <td class="center">4</td>
                                        <td class="center">
											<a href="#" class="btn btn-danger btn-xs" role="button">
											<i class="fa fa-fw fa-stop"></i></a>
											<a href="#" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Usersss" role="button"><i class="fa fa-fw fa-trash"></i></a>
										</td>
                                    </tr>
									<tr class="odd gradeX">
                                        <td>Add</td>
                                        <td>Internet Explorer 4.0</td>
                                        <td>Win 95+</td>
                                        <td class="center">4</td>
                                        <td class="center">
											<a href="#" class="btn btn-danger btn-xs" role="button">
											<i class="fa fa-fw fa-stop"></i></a>
											<a href="#" class="btn btn-default btn-xs" role="button"><i class="fa fa-fw fa-cog"></i></a>
											<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="Usexxx" role="button"><i class="fa fa-fw fa-trash"></i></a>
										</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

	<!-- /#Message BOx -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content panel-danger">
		  <div class="modal-header panel-heading">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Confirm to delete user</h4>
		  </div>
		 
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Confirm</button>
		  </div>
		</div>
	  </div>
	</div>
	<!-- /#Message BOx -->
   
   <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>


    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
	
	$('#exampleModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var recipient = button.data('whatever') // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)
	  modal.find('.modal-title').text('Confirm to delete project (' + recipient + ')')
	  modal.find('.modal-body input').val(recipient)
	})
    </script>

</body>

</html>
