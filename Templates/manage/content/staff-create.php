<!DOCTYPE html>
<html lang="en">

<head>
   
</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add a staff</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-radius: 0;">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								<h3>Account Profile</h3>
                                    <form role="form">
									
                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input class="form-control" type="email">
                                           
                                        </div>
										<div class="form-group">
                                            <label>Title</label>
                                            <select class="form-control">
                                                <option>Mr</option>
                                                <option>Mrs</option>
                                                <option>Dr</option>
                                                <option>Ms</option>
                                                <option>Miss</option>
                                            </select>
                                        </div>
										 <div class="form-group">
                                            <label>Deakin ID</label>
                                            <input class="form-control" type="email">
                                            <p class="help-block">If you have please input your ID number</p>
                                        </div>
										<div class="form-group">
                                            <label>Preferred Name</label>
                                            <input class="form-control" placeholder="Enter Last Name">
                                        </div>
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input class="form-control" placeholder="Enter Last Name">
                                        </div>
										<div class="form-group">
                                            <label>Last Name</label>
                                            <input class="form-control" placeholder="Enter Last Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Profile Image</label>
                                            <input type="file">
                                        </div>
                                        
                                    
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <h3>Account Setting</h3>
                                   
                                    
                                   
                                        <div class="form-group">
                                            <label class="control-label" for="inputSuccess">Email Address</label>
                                            <input type="text" class="form-control" id="inputSuccess">
											 <p class="help-block">Email address as your username</p>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="inputWarning">Password</label>
                                            <input type="text" class="form-control" id="inputWarning">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="inputError">Comfirm Password</label>
                                            <input type="text" class="form-control" id="inputError">
                                        </div>
										
										
                                    
                                </div>
								<div class="col-lg-12">
									<button type="submit" class="btn btn-success pull-right" style="margin-left: 20px;">Confirm</button>
                                    <button type="reset" class="btn btn-default pull-right">Reset</button>
								</div>
								</form>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
