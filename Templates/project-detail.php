<?php
session_start(); 
$id= $_REQUEST['id'];


/* Set oracle user login and password info */
$dbuser = "hyou"; /* your deakin login */
$dbpass = "Yhx91311"; /* your oracle access password */
$db = "SSID";
$connect = oci_connect($dbuser, $dbpass, $db);

if (!$connect) {
echo "An error occurred connecting to the database";
exit;
}
$query = "SELECT * FROM dig_project where id = $id";

$result = oci_parse($connect, $query);

if(!$result) {
echo "An error occurred in parsing the sql string.\n";
exit;
}
else oci_execute($result);
echo $id;

while(oci_fetch_array($result)) {
			$title = oci_result($result,2);
			$key1 = oci_result($result,3);
			$key2 = oci_result($result,4);
			$contacts = oci_result($result,5);
			$category = oci_result($result,6);
			$pdf = oci_result($result,7);
			$overview = oci_result($result,8);
			$id = oci_result($result,1);
}

$_SESSION['pdf'] = $pdf;
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Project:Monetisation Strategy: ABC’s Alphabet Safari</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	 <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">Start Bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#project">Project</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#news">News</a>
                    </li>
					<li class="page-scroll">
                        <a href="#news">Forum</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <!-- Image Background Page Header -->
    <!-- Note: The background image is set within the business-casual.css file. -->
    <header class="business-header" style='background: transparent url("./img/appdevelopmet.jpg") no-repeat scroll center center;'>
	 <div class="business-header" style="background-color: rgba(0, 0, 0, 0.32); text-align: left;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class=""><?php echo $title; ?></h1>
					<hr>
					<h5 class=""><?php echo $key1; ?></h5>
					<h5 class=""><?php echo $key2; ?></h5>
					<h5 class=""><?php echo $category; ?></h5>
                </div>
            </div>
        </div>
        </div>
    </header>

	<section style="padding: 30px;">
		<!-- Page Content -->
		<div class="container">

			

			<div class="row">
				<div class="col-sm-7">
				
					<h4>Project Overview:</h4>
					<hr>
					<p  class="small"><?php echo $overview; ?></p>
					
				</div>
				<div class="col-sm-5">
					<h4>Project contacts</h4>
					<hr>
					<p class="small"><?php echo $contacts; ?></p>
					
				</div>
			</div>
			</br>
			</br>
			</br>
			<div class="row">
				<div class="panel panel-default" style="border-color: white; border-radius: 0px;">
					<div class="panel-heading" style="padding-left: 0px; padding-right: 0px; background-color: white;">
						<h3 class="panel-title">
							<ul class="nav nav-pills nav-justified">
							  <li role="presentation" class="active"><a href="project-detail.php" style="background-color: rgb(166, 188, 105); border-radius: 0px;">Project Description</a></li>
							  <li role="presentation"><a href="project-resources.php">Project Resources</a></li>
							  <li role="presentation"><a href="project-discussions.php">Discussions</a></li>
							</ul>
						</h3>
					</div>
					<div>
						<div style="height:800px">
						
						<embed  class="col-sm-12" style="height:800px" src="pdf/web/viewer.php"  />
						</div>
					</div>
				</div>
			</div>
		</div>
    <!-- /.container -->
	</section>
	
    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>3481 Melrose Place
                            <br>Beverly Hills, CA 90210</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <img src="img/portfolio/logo.png" class="img-responsive img-centered" alt="">
                    </div>
                </div>
            </div>
        </div>
       
    </footer>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>
	



</body>

</html>
