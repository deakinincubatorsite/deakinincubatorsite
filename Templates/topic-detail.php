<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Freelancer - Start Bootstrap Theme</title>

  <!-- Bootstrap Core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Theme CSS -->
  <link href="css/freelancer.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body id="page-top" class="index">

  <!-- Navigation -->
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="#page-top">Start Bootstrap</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="hidden">
            <a href="#page-top"></a>
          </li>
          <li class="page-scroll">
            <a href="#project">Project</a>
          </li>
          <li class="page-scroll">
            <a href="#news">News</a>
          </li>
          <li class="page-scroll">
            <a href="">Forum</a>
          </li>

        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>

  <!-- Header -->
  <header style="background: #a6bc69;">
    <div class="container">
      <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">

          <div class="intro-text">

            <div class="form-group col-xs-11 floating-label-form-group controls" style="border-left: 0px none;">

              <span><input class="form-control fa fa-search"id="search"></span>

              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group col-xs-1 floating-label-form-group controls" style="border-left: 0px none;">
              <i class="fa fa-search fa-3x" style="font-size:3.15em" aria-hidden="true"></i>
            </div>

          </div>
        </div>
        <div class="col-lg-2">
        </div>
      </div>
    </div>
  </header>

  <!-- Portfolio Grid Section -->
  <section id="portfolio" style="padding-top: 26px;">
    <div class="container">
      <div  class="col-lg-12" >
        <div class="row">
          <div class="col-sm-12 text-left">
            <h3>DIG T3 Meeting- 30/9/15</h3>
            <hr class="primary">
          </div>
        </div>

        <div class="row" style="margin: 0px;">
          <div class='col-lg-12 portfolio-item' style='background-color:white;'>
            <div class='col-lg-2'>
              <div class='row-sm-5'>
                <image style="width: 50px;" src ="https://d2l.deakin.edu.au/d2l/common/viewprofileimage.d2l?oi=6605&ui=134644&s=100&lm=635597530511230000&v=11&cui=114172&v=10.6.0.2348-197&v=10.6.0.2348-197"/>
              </div>
              <div>
                <p class='small'>Greg Bowtell</p>
              </div>
            </div>
            <div class='col-sm-9' style='' >
              <div class='col-sm-12 portfolio-item' style="margin-bottom: 0px;background-color:white;">
                <div class='col-sm-12' style='  padding: 0px; border-buttom: 1px solid rgba(0, 0, 0, 0.09);' >
                  <div class='col-sm-12 carboard-news-title'>
                    <a href='topic-detail.php?id=$id'><h5>CHRIS WARD posted 28 September, 2015 9:09 AM</h5></a>
                  </div>

                  <div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); overflow:hidden;'>
                    <p>Hi All,
                      <br/>
                      <br/>I would love to have gone to or Skyped into the meeting this Wednesday, however as I am working full time as I study Cloud, I unfortunately wont be finished work in time for this meeting.
                      <br/>
                      <br/>Will this meeting be recorded for me to look over in my own time?
                      <br/>
                      <br/>Kind regards,

                      Chris.</P>
                    </div>

                  </div>
                </div>

              </div>
              <div class='col-sm-1'>
                <button>reply</button>
              </div>
            </div>
            <div class='col-sm-12'>
              <hr class='primary'>
            </div>
          </div>
          <div class="row" style="margin: 0px;">
            <div class='col-lg-12 portfolio-item' style='background-color:white;'>
              <div class='col-lg-2'>
                <div class='row-sm-5'>
                  <image style="width: 50px;" src ="https://d2l.deakin.edu.au/d2l/common/viewprofileimage.d2l?oi=6605&ui=134644&s=100&lm=635597530511230000&v=11&cui=114172&v=10.6.0.2348-197&v=10.6.0.2348-197"/>
                </div>
                <div>
                  <p class='small'>Greg Bowtell</p>
                </div>
              </div>
              <div class='col-sm-9' style='' >
                <div class='col-sm-12 portfolio-item' style="margin-bottom: 0px;background-color:white;">
                  <div class='col-sm-12' style='  padding: 0px; border-buttom: 1px solid rgba(0, 0, 0, 0.09);' >
                    <div class='col-sm-12 carboard-news-title'>
                      <a href='topic-detail.php?id=$id'><h5>CHRIS WARD posted 28 September, 2015 9:09 AM</h5></a>
                    </div>

                    <div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); overflow:hidden;'>
                      <p>Hi All,
                        <br/>
                        <br/>I would love to have gone to or Skyped into the meeting this Wednesday, however as I am working full time as I study Cloud, I unfortunately wont be finished work in time for this meeting.
                        <br/>
                        <br/>Will this meeting be recorded for me to look over in my own time?
                        <br/>
                        <br/>Kind regards,

                        Chris.</P>
                      </div>

                    </div>
                  </div>

                </div>
                <div class='col-sm-1'>
                  <button>reply</button>
                </div>
              </div>
              <div class='col-sm-12'>
                <hr class='primary'>
              </div>
            </div>


          </section>




          <!-- Footer -->
          <footer class="text-center">
            <div class="footer-above">
              <div class="container">
                <div class="row">
                  <div class="footer-col col-md-4">
                    <h3>Location</h3>
                    <p>3481 Melrose Place
                      <br>Beverly Hills, CA 90210</p>
                    </div>
                    <div class="footer-col col-md-4">
                      <h3>Around the Web</h3>
                      <ul class="list-inline">
                        <li>
                          <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                        </li>
                        <li>
                          <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                        </li>
                        <li>
                          <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                        </li>
                        <li>
                          <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                        </li>
                        <li>
                          <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                        </li>
                      </ul>
                    </div>
                    <div class="footer-col col-md-4">
                      <img src="img/portfolio/logo.png" class="img-responsive img-centered" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="footer-below">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12">
                      Copyright &copy; Your Website 2016
                    </div>
                  </div>
                </div>
              </div>
            </footer>

            <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
            <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
              <a class="btn btn-primary" href="#page-top">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>



            <!-- jQuery -->
            <script src="vendor/jquery/jquery.min.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

            <!-- Plugin JavaScript -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

            <!-- Contact Form JavaScript -->
            <script src="js/jqBootstrapValidation.js"></script>
            <script src="js/contact_me.js"></script>

            <!-- Theme JavaScript -->
            <script src="js/freelancer.min.js"></script>

          </body>

          </html>
