<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'descriptionformat') ?>

    <?= $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'catagroyid') ?>

    <?php // echo $form->field($model, 'ownerid') ?>

    <?php // echo $form->field($model, 'path') ?>

    <?php // echo $form->field($model, 'timecreated') ?>

    <?php // echo $form->field($model, 'timemodified') ?>

    <?php // echo $form->field($model, 'usermodified') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
