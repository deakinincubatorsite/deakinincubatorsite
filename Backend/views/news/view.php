<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                 'attribute' => 'catagroyid',
                 'label' => 'Category',
                 'value' => $model->category->fullname,
            ],
            'subject',
            'message:ntext',
            'messageformat',
            //'messagetrust',
            //'canreply',
            //'attachment',
            //'readercount',
            //'status',
            'created',
            'modified',
            [
                 'attribute' => 'userid',
                 'label' => 'Modified by',
                 'value' => $model->user->username,
            ],
        ],
    ]) ?>

</div>
