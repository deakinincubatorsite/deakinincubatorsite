<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\models\User;
use app\models\NewsCategory;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;

$usernameList = ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username');
$categoryList = ArrayHelper::map(NewsCategory::find()->asArray()->all(), 'id', 'fullname');
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'catagroyid', 
                'filter' => $categoryList, 
                'label' => 'Category', 
                'value' => function ($model, $index, $widget) { return $model->category->fullname; }, 
            ],
            'subject',
            'created',
            'modified',
            [
                'attribute' => 'userid', 
                'filter' => $usernameList, 
                'label' => 'Modified by', 
                'value' => function ($model, $index, $widget) { return $model->user->username; }, 
            ],
            // 'message:ntext',
            // 'messageformat',
            // 'messagetrust',
            // 'canreply',
            // 'attachment',
            // 'readercount',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
