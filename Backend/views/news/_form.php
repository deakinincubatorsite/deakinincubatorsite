<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\ckeditor\CKEditor;
use app\models\User;
use app\models\NewsCategory;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */

$categoryList = ArrayHelper::map(NewsCategory::find()->asArray()->all(), 'id', 'fullname');
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'catagroyid')->dropDownList($categoryList) ?>

    <?= $form->field($model, 'userid')->hiddenInput(['value' => User::findByUsername(Yii::$app->user->identity->username)->id])->label(false) ?>

    <?= $form->field($model, 'created')->hiddenInput(['value' => 'default'])->label(false)?>

    <?= $form->field($model, 'modified')->hiddenInput(['value' => 'default'])->label(false) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>

    <?= $form->field($model, 'messageformat')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>

    <?= $form->field($model, 'messagetrust')->hiddenInput(['value' => 0])->label(false) ?>

    <?= $form->field($model, 'canreply')->hiddenInput(['value' => 0])->label(false) ?>

    <?= $form->field($model, 'attachment')->hiddenInput(['value' => 0])->label(false) ?>

    <?= $form->field($model, 'readercount')->hiddenInput(['value' => 0])->label(false) ?>

    <?= $form->field($model, 'status')->hiddenInput(['value' => 0])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
