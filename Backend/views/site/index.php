<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'DIG - Deakin Incubator Group';
?>
  <div class="col-lg-9">
    <div class="row">
      <div class="col-lg-12 text-left">
        <h2>News</h2>
        <hr class="primary">
      </div>
    </div>

    <div class="row" style="margin: 0px;">


      <div class="col-sm-12 portfolio-item" style="background-color:white;">
        <div class="col-sm-12" style="  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);">
          <div class="col-sm-12 carboard-news-title">
            <a href="news-detail.php?id=1"><h3>DIG: Opportunities</h3></a>
          </div>

          <div class="col-sm-12 carboard-news-detail" style="color: rgb(53, 52, 52); max-height:150px; overflow:hidden;">
            <p></p>
            <p>
            </p>
            <p>There are a number of opportunities for involvement with the Deakin Incubator Group (DIG) through several open projects at present.</p>
            <p>&nbsp; - paid work opportunities with the Data Instruments project (contact me - <a href="\&quot;mailto:shaun.bangay@deakin.edu.au\&quot;">shaun.bangay@deakin.edu.au</a>&nbsp;by 24/3/2016 if you're intending to apply), and see the application
              process and opportunities described in the project's discussion forum on the DIG site).</p>
            <p>&nbsp; - opportunities to work on an industry relevant project with some experienced and influential people in the IT industry. The benefit of a share in the startup venture are only part of the reason to get involved. There is also opportunity
              to establish a significant professional network. See the details of the radIQals project on the DIG site. Please let me - <a href="\&quot;mailto:shaun.bangay@deakin.edu.au\&quot;">shaun.bangay@deakin.edu.au</a>&nbsp;- know before 25/3/2016
              if you're interested in being involved, and we'll send out further details of the application process after that.</p>
            <p>&nbsp; - the Tantalizing team has released their first episode, and is starting work on the next release. There is an opening for a marketing coordinator. Please see the Tantalizing discussion forum on the DIG site for more details.</p>
            <p>Cheers</p>
            <p>Shaun</p>
            <p>
            </p>
            <p>
              <br>
            </p>
            <p></p>

          </div>
          <div style="height:5px"> </div>
          <div class="col-sm-12" style="padding-top: 10px;">
            <a href="news-detail.php?id=1">Read More..</a>
            <hr class="primary">
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="padding-left:0px">
              <p class="small pull-left"><a href="#">Shaun</a></p>
            </div>
            <div class="col-sm-6">
              <p class="small pull-right">Posted 21 March, 2016 8:05 AM</p>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-12 portfolio-item" style="background-color:white;">
        <div class="col-sm-12" style="  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);">
          <div class="col-sm-12 carboard-news-title">
            <a href="news-detail.php?id=2"><h3>Project Tantalizing: Game release</h3></a>
          </div>

          <div class="col-sm-12 carboard-news-detail" style="color: rgb(53, 52, 52); max-height:150px; overflow:hidden;">
            <p></p>
            <p>
            </p>
            <p>The Tantalizing team are pleased to announce the release of their first episode as a standalone game in the Google play store. The game is titled&nbsp;<strong>"Till the End of Slime"</strong>&nbsp;and can be found by searching for that title
              (the quotes help :-)), or hopefully by accessing the link:</p>
            <p><a href="\&quot;https://play.google.com/store/apps/details?id=dig.tantalizing.tantendofslime\&quot;">https://play.google.com/store/apps/details?id=dig.tantalizing.tantendofslime</a></p>
            <p>The game is free to play (genuinely, with no sneaky attempts to extort money in other ways) and we invite you to try it out.&nbsp;</p>
            <p>Cheers</p>
            <p>Shaun</p>
            <p>
            </p>
            <p>
              <br>
            </p>
            <p></p>

          </div>
          <div style="height:5px"> </div>
          <div class="col-sm-12" style="padding-top: 10px;">
            <a href="news-detail.php?id=2">Read More..</a>
            <hr class="primary">
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="padding-left:0px">
              <p class="small pull-left"><a href="#">Shaun</a></p>
            </div>
            <div class="col-sm-6">
              <p class="small pull-right">Posted 09 March, 2016 8:29 AM</p>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-12 portfolio-item" style="background-color:white;">
        <div class="col-sm-12" style="  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);">
          <div class="col-sm-12 carboard-news-title">
            <a href="news-detail.php?id=3"><h3>DIG project: Data Instruments project released</h3></a>
          </div>

          <div class="col-sm-12 carboard-news-detail" style="color: rgb(53, 52, 52); max-height:150px; overflow:hidden;">
            <p></p>
            <p>
            </p>
            <p>Details for the Data Instruments project have been posted on the DIG CloudDeakin site. There are opportunities for paid work on this project. Please see the associated description document and discussion forum for further details.
            </p>
            <p>Cheers</p>
            <p>Shaun</p>
            <p>
            </p>
            <p>
              <br>
            </p>
            <p></p>

          </div>
          <div style="height:5px"> </div>
          <div class="col-sm-12" style="padding-top: 10px;">
            <a href="news-detail.php?id=3">Read More..</a>
            <hr class="primary">
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="padding-left:0px">
              <p class="small pull-left"><a href="#">Shaun</a></p>
            </div>
            <div class="col-sm-6">
              <p class="small pull-right">Posted 09 March, 2016 8:27 AM</p>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-12 portfolio-item" style="background-color:white;">
        <div class="col-sm-12" style="  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);">
          <div class="col-sm-12 carboard-news-title">
            <a href="news-detail.php?id=4"><h3>Storypod is go</h3></a>
          </div>

          <div class="col-sm-12 carboard-news-detail" style="color: rgb(53, 52, 52); max-height:150px; overflow:hidden;">
            <p></p>
            <p>
            </p>
            <p>Hi all,</p>
            <p>The Storypod project has officially launched and the team met with the client last week.&nbsp; They are now tackling their first set of tasks.&nbsp; I'll post updates as they happen, stay tuned.</p>
            <p>Cheers,
              <br>Greg</p>
            <p>
              <br>
            </p>
            <p></p>

          </div>
          <div style="height:5px"> </div>
          <div class="col-sm-12" style="padding-top: 10px;">
            <a href="news-detail.php?id=4">Read More..</a>
            <hr class="primary">
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="padding-left:0px">
              <p class="small pull-left"><a href="#">Greg</a></p>
            </div>
            <div class="col-sm-6">
              <p class="small pull-right">03 March, 2016 11:45 AM</p>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-12 portfolio-item" style="background-color:white;">
        <div class="col-sm-12" style="  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);">
          <div class="col-sm-12 carboard-news-title">
            <a href="news-detail.php?id=5"><h3>Storypod: final opportunity to participate in Stage 1</h3></a>
          </div>

          <div class="col-sm-12 carboard-news-detail" style="color: rgb(53, 52, 52); max-height:150px; overflow:hidden;">
            <p></p>
            <p>We're reopening the recruiting for Stage 1 of Storypod (see earlier posts for the details). We have a number of interested parties but do still welcome anyone else who would like to express interest in being part of the project. The closing
              date for applications is now 18 February 2016 (only a couple of days away).</p>
            <p>Please see the postings in the StoryPod discussion forum on the DIG CloudDeakin site for more details of the application process, and for background on the StoryPod project.</p>
            <p>Cheers</p>
            <p>Shaun</p>
            <p>
              <br>
            </p>
            <p></p>

          </div>
          <div style="height:5px"> </div>
          <div class="col-sm-12" style="padding-top: 10px;">
            <a href="news-detail.php?id=5">Read More..</a>
            <hr class="primary">
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="padding-left:0px">
              <p class="small pull-left"><a href="#">Shaun</a></p>
            </div>
            <div class="col-sm-6">
              <p class="small pull-right">16 February, 2016 12:24 PM</p>
            </div>
          </div>

        </div>
      </div>
      <div class="col-sm-12 portfolio-item" style="background-color:white;">
        <div class="col-sm-12" style="  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);">
          <div class="col-sm-12 carboard-news-title">
            <a href="news-detail.php?id=6"><h3>Paid project opportunities: web site development with social media integration</h3></a>
          </div>

          <div class="col-sm-12 carboard-news-detail" style="color: rgb(53, 52, 52); max-height:150px; overflow:hidden;">
            <p></p>
            <p>The Deakin Incubator group is looking to recruit for some paid positions (1-2 weeks during late December, early January). If you have skills relating to web site development, particularly involving integrating data from Twitter or other social
              media sites, and would like to be considered, please contact Shaun Bangay (<a href="\&quot;mailto:shaun.bangay@deakin.edu.au\&quot;">shaun.bangay@deakin.edu.au</a>) to describe your relevant expertise and to express your interest.&nbsp;</p>
            <p>Regards</p>
            <p>Shaun</p>
            <p>
              <br>
            </p>
            <p></p>

          </div>
          <div style="height:5px"> </div>
          <div class="col-sm-12" style="padding-top: 10px;">
            <a href="news-detail.php?id=6">Read More..</a>
            <hr class="primary">
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="padding-left:0px">
              <p class="small pull-left"><a href="#">Shaun</a></p>
            </div>
            <div class="col-sm-6">
              <p class="small pull-right">15 December, 2015 12:23 PM</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>


  <div class="col-lg-3">

    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>Projects</h2>
        <hr class="primary">
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">Game Development</i>
            </div>
          </div>
          <?= Html::img('@web/images/cabin.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">Multimedia</i>
            </div>
          </div>
          <?= Html::img('@web/images/cake.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">Software Development</i>
            </div>
          </div>
          <?= Html::img('@web/images/circus.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">App Development</i>
            </div>
          </div>
          <?= Html::img('@web/images/game.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">Robotics</i>
            </div>
          </div>
          <?= Html::img('@web/images/safe.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">Wearable Computing</i>
            </div>
          </div>
          <?= Html::img('@web/images/submarine.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">Security</i>
            </div>
          </div>
          <?= Html::img('@web/images/cabin.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
      <div class="col-sm-12 portfolio-item">
        <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
          <div class="caption">
            <div class="caption-content">
              <i class="fa">IT for Health</i>
            </div>
          </div>
          <?= Html::img('@web/images/cabin.png', ['class' => 'img-responsive','alt' => '']) ?>
        </a>
      </div>
    </div>


  </div>