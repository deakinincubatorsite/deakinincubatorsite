<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-3"></div>
<div class="col-lg-6">
    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
        <?= $form->field($model, 'alternatename') ?>
        <?= $form->field($model, 'firstname') ?>
        <?= $form->field($model, 'lastname') ?>
        <?= $form->field($model, 'title')->dropDownList(User::$titles) ?>
        <?= $form->field($model, 'schoolid') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-signup -->
<div class="col-lg-3"></div>