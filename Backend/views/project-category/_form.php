<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\ckeditor\CKEditor;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\ProjectCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->hiddenInput(['value' => '0'])->label(false) ?>

    <?= $form->field($model, 'descriptionformat')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard'
    ]) ?>

    <?= $form->field($model, 'timecreated')->hiddenInput(['value' => 'default'])->label(false) ?>

    <?= $form->field($model, 'timemodified')->hiddenInput(['value' => 'default'])->label(false) ?>

    <?= $form->field($model, 'usermodified')->hiddenInput(['value' => User::findByUsername(Yii::$app->user->identity->username)->id])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
