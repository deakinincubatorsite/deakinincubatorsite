<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewsCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'descriptionformat') ?>

    <?= $form->field($model, 'timecreated') ?>

    <?php // echo $form->field($model, 'timemodified') ?>

    <?php // echo $form->field($model, 'usermodified') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
