<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Html::cssFile('@web/css/font-awesome.css', []) ?>
    <?= Html::cssFile('@web/css/freelancer.css', []) ?>
</head>
<body>
<?php $this->beginBody() ?>
 <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="<?= Url::to(['site/index']) ?>">DIG</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <?= Html::a('Project', ['site/project']) ?>
                    </li>
                    <li class="page-scroll">
                        <?= Html::a('News', ['site/news']) ?>
                    </li>
					<li class="page-scroll">
                        <?= Html::a('Forum', ['site/forum']) ?>
                    </li>
                    <?php if (Yii::$app->user->isGuest) {?> 
                        <li class="page-scroll">
                            <?= Html::a('Signup', ['site/signup']) ?>
                        </li>
                    <?php } ?>
					<li class="page-scroll">
                        <?=  Yii::$app->user->isGuest ? Html::a('Login', ['site/login']): Html::a('Management', ['backend/index']) ?>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header style="background: #a6bc69;">
        <div class="container">
            <div class="row">
			 <div class="col-lg-2">
			 </div>
                <div class="col-lg-8">
                   
                    <div class="intro-text">
					
					 <div class="form-group col-xs-11 floating-label-form-group controls" style="border-left: 0px none; padding-bottom: 0px;">
					
						<input class="form-control fa fa-search"id="search" style="color: white; font-size: 1.4em;">
						
						<p class="help-block text-danger" style="margin: 0px;"></p>
					</div>
					<div class="form-group col-xs-1 floating-label-form-group controls" style="border-left: 0px none;">
						<button  style="border:1px solid transparent; background-color: transparent;"><i class="fa fa-search" style="font-size:1.7em" aria-hidden="true"></i></button>
					</div>
                       
                    </div>
                </div>
				<div class="col-lg-2">
			 </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <?= $content ?>
        </div>
    </section>


    

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
					<div class=" col-md-6">
                        <?= Html::img('@web/images/logo.png', ['style' => 'max-height:80px', 'class' => 'img-responsive img-centered pull-left','alt' => 'DIG']) ?>
                    </div>
                    
                    <div class=" col-md-6">
                        <h3>About Us</h3>
                       
                    </div>
                    
                </div>
				
            </div>
			<hr class="primary" style="border-top: 1px solid rgb(191, 200, 171);">
				<div class="row">
                    <div class=" col-lg-12">
                        Copyright &copy; Deakin Incubator Group 2016
                    </div>
                </div>
        </div>
      
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?= Html::jsFile('@web/js/freelancer.js') ?>
<?php $this->endPage() ?>
