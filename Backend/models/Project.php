<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dig_core_project".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $description
 * @property string $descriptionformat
 * @property string $location
 * @property integer $catagroyid
 * @property integer $ownerid
 * @property string $path
 * @property string $timecreated
 * @property string $timemodified
 * @property integer $usermodified
 * @property string $status
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dig_core_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'description', 'location', 'catagroyid', 'ownerid', 'path', 'timecreated', 'timemodified', 'usermodified'], 'required'],
            [['description', 'path'], 'string'],
            [['catagroyid', 'ownerid', 'usermodified'], 'integer'],
            [['timecreated', 'timemodified'], 'safe'],
            [['fullname', 'descriptionformat', 'location', 'status'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'description' => 'Description',
            'descriptionformat' => 'Descriptionformat',
            'location' => 'Location',
            'catagroyid' => 'Catagroyid',
            'ownerid' => 'Ownerid',
            'path' => 'Path',
            'timecreated' => 'Timecreated',
            'timemodified' => 'Timemodified',
            'usermodified' => 'Usermodified',
            'status' => 'Status',
        ];
    }
}
