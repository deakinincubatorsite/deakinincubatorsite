<?php

namespace app\models;

use Yii;
use yii\base\Model;

use app\models\User;
/**
 * SignupForm is the model behind the signup form.
 */
class SignupForm extends Model
{
    public $username;
    public $password;
    public $confirmPassword;
    public $alternatename;
    public $firstname;
    public $lastname;
    public $title;
    public $schoolid;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password','confirmPassword', 'firstname', 'lastname', 'schoolid'], 'required'],
            [['schoolid'], 'integer'],
            [['alternatename', 'title'], 'string'],
            [['username', 'password', 'confirmPassword', 'firstname', 'lastname'], 'string', 'max' => 64],
            ['username', 'validateUsername'],
            ['confirmPassword', 'compare', 'compareAttribute'=>'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'password' => 'Password',
            'confirmPassword' => 'Comfirm Password',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'schoolid' => 'School ID',
            'alternatename' => 'Preferred name',
            'title' => 'Title',
        ];
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateUsername($attribute, $params)
    {
        $user = User::findByUsername($this->$attribute);
        if ($user!=null) {
            $this->addError($attribute, 'Username is already taken.');
        }
    }
}
