<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form about `app\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catagroyid', 'userid', 'messagetrust', 'attachment', 'readercount'], 'integer'],
            [['created', 'modified', 'subject', 'message', 'messageformat', 'canreply', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'catagroyid' => $this->catagroyid,
            'userid' => $this->userid,
            'created' => $this->created,
            'modified' => $this->modified,
            'messagetrust' => $this->messagetrust,
            'attachment' => $this->attachment,
            'readercount' => $this->readercount,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'messageformat', $this->messageformat])
            ->andFilterWhere(['like', 'canreply', $this->canreply])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
