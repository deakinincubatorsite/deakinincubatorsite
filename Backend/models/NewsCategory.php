<?php

namespace app\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "dig_core_artical_catagroy".
 *
 * @property integer $id
 * @property string $fullname
 * @property integer $description
 * @property string $descriptionformat
 * @property string $timecreated
 * @property string $timemodified
 * @property integer $usermodified
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dig_core_artical_catagroy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'description', 'timecreated', 'timemodified', 'usermodified'], 'required'],
            [['description', 'usermodified'], 'integer'],
            [['timecreated', 'timemodified'], 'safe'],
            [['fullname', 'descriptionformat'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Name',
            'description' => 'Description',
            'descriptionformat' => 'Description',
            'timecreated' => 'Created on',
            'timemodified' => 'Modified on',
            'usermodified' => 'Modified by',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'timecreated',
                'updatedAtAttribute' => 'timemodified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return User::findIdentity($this->usermodified);
    }
}
