<?php

namespace app\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "dig_core_news".
 *
 * @property integer $id
 * @property integer $catagroyid
 * @property integer $userid
 * @property string $created
 * @property string $modified
 * @property string $subject
 * @property string $message
 * @property string $messageformat
 * @property integer $messagetrust
 * @property string $canreply
 * @property integer $attachment
 * @property integer $readercount
 * @property string $status
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dig_core_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catagroyid', 'userid', 'created', 'modified', 'subject', 'message', 'messagetrust', 'attachment', 'readercount'], 'required'],
            [['catagroyid', 'userid', 'messagetrust', 'attachment', 'readercount'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['message'], 'string'],
            [['subject', 'messageformat', 'canreply', 'status'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catagroyid' => 'Category',
            'userid' => 'Modified By',
            'created' => 'Created On',
            'modified' => 'Modified On',
            'subject' => 'Subject',
            'message' => 'Message',
            'messageformat' => 'Messageformat',
            'messagetrust' => 'Messagetrust',
            'canreply' => 'Canreply',
            'attachment' => 'Attachment',
            'readercount' => 'Reader count',
            'status' => 'Status',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return User::findIdentity($this->userid);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'catagroyid']);
    }
}
