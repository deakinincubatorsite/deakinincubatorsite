<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dig_core_account_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $createpassword
 * @property string $firstname
 * @property string $lastname
 * @property integer $schoolid
 * @property string $avatar
 * @property string $lang
 * @property string $theme
 * @property string $timezone
 * @property string $city
 * @property string $country
 * @property string $middlename
 * @property string $alternatename
 * @property string $title
 * @property integer $isStaff
 * @property string $accessToken
 * @property string $authKey
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dig_core_account_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'firstname', 'lastname', 'schoolid', 'accessToken', 'authKey'], 'required'],
            [['schoolid', 'isStaff'], 'integer'],
            [['avatar', 'lang', 'theme', 'timezone', 'city', 'country', 'middlename', 'alternatename', 'title'], 'string'],
            [['username', 'password', 'createpassword', 'firstname', 'lastname'], 'string', 'max' => 64],
            [['accessToken', 'authKey'], 'string', 'max' => 32],
            ['username', 'validateUsername'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'createpassword' => 'Createpassword',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'schoolid' => 'School ID',
            'avatar' => 'Avatar',
            'lang' => 'Lang',
            'theme' => 'Theme',
            'timezone' => 'Timezone',
            'city' => 'City',
            'country' => 'Country',
            'middlename' => 'Middle name',
            'alternatename' => 'Preferred name',
            'title' => 'Title',
            'isStaff' => 'Is Staff',
            'accessToken' => 'Access Token',
            'authKey' => 'Auth Key',
        ];
    }

    private static $users = [
        '0' => [
        'id' => '0',
        'username' => 'admin',
        'password' => '21232f297a57a5a743894a0e4a801fc3',
        'authKey' => 'test100key',
        'accessToken' => '100-token',
        ]
    ];

    public static $titles = [
            "Mr" => "Mr",
            "Mrs" => "Mrs",
            "Dr" => "Dr",
            "Ms" => "Ms"
        ];

    public static function findIdentity($id){
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : static::findOne($id);
    }
    
    public static function findIdentityByAccessToken($token, $type = null){
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        throw static::findByAttributes(array('accessToken'=>$token));
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getAuthKey(){
        return $this->authKey;//Here I return a value of my authKey column
    }
    
    public function validateAuthKey($authKey){
        return $this->authKey === $authKey;
    }
    public static function findByUsername($username){
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }
        return self::findOne(['username'=>$username]);
    }
    
    public function validatePassword($password){
        return $this->password === md5($password);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($insert)
            {
                $this->password = md5($this->password);
                $this->accessToken = Yii::$app->getSecurity()->generateRandomString();
                $this->authKey = Yii::$app->getSecurity()->generateRandomString();
            }
            else
            {
                $current = self::find()->where(['id' => $this->id])->one();
                if($this->password != $current->password)
                    $this->password = md5($this->password);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateUsername($attribute, $params)
    {
        $user = User::findByUsername($this->$attribute);
        if ($user!=null) {
            $this->addError($attribute, 'Username is already taken.');
        }
    }
}
