<!DOCTYPE html>
<html lang="en">

<head>
   
</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create a category for project</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-radius: 0;">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								<h3></h3>
                                    <form role="form">
									
                                        <div class="form-group">
                                            <label>Category Name</label>
                                            <input class="form-control" type="email">
                                           
                                        </div>
                                        <div class="form-group">
                                            <label>Category Default Background</label>
                                            <input type="file">
                                        </div>
                                        
                                    
                                </div>
                                
								<div class="col-lg-12">
									<button type="submit" class="btn btn-success pull-right" style="margin-left: 20px;">Confirm</button>
                                    <button type="reset" class="btn btn-default pull-right">Reset</button>
								</div>
								</form>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
