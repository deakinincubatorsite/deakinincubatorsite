<?php
@session_start;
?>
<!DOCTYPE html>
<html lang="en">

<head>
   
    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Project Category</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			<?php 
			if(isset($_SESSION['upload256']))
			{
				$result = $_SESSION['upload256'];
				if($result == "0")
					{
						echo "
							<div class='alert alert-success' role='alert'>
							 <p>Your project category create successful</p>
							</div>";
							
					}
						
					if($result == "1")
					{
						echo "
						<div class='alert alert-danger' role='alert'>
						 <p>Your project category create unsuccessful</p>
						</div>";
					}
					
				unset($_SESSION['upload256']); 
			}
			
				
					
				
			
			?>
			
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-radius: 0px;" >
                        <div class="panel-heading" style="padding: 0px;">
							<a href=""class="btn btn-primary"  data-toggle="modal"  data-target="#categoryAddModal" style="border-radius: 0px; margin: 0px; background-color: rgb(166, 188, 105); border-color: aliceblue; width:100%" type="button" role="button">Add a category</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Project Count</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
									include("../config/config.php");
									$sql = "SELECT * FROM dig_core_project_catagroy";
									$result = $con->query($sql);
									while($row =$result->fetch_array() ){
									   //echo ($row[0])."<br>";

									   $title = $row['fullname'];
									   $id = $row['id'];
									   if($title != "")
										{
											echo "
											<tr class='odd gradeX'>
												<td>$title</td>
												<td>13</td>
												<td class='center'>
													
													<a href='#' class='btn btn-default btn-xs update'  data-target='#categoryUpdateModal' data-myid='$id' data-mydata='$title' role='button'><i class='fa fa-fw fa-cog'></i></a>
													<a href='#' class='btn btn-default btn-xs delete' data-toggle='modal' data-target='#exampleModal' data-myid='$id' data-mydata='$title' role='button'><i class='fa fa-fw fa-trash'></i></a>
												</td>
											</tr>
											";
												
										}
									}
								
								?>
                                  
					
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	
	<!-- /#Message BOx -->
	<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Delete Alert</h4>
					  </div>
					  <div class="modal-body">
						<form name="catedelete" method="POST" action="../manage/push/project-category-delete.php"> 
						  <div class="form-group">
							<h4 class="modal-title" id="aleartLable">Confirm delete project </h4>
							<input class="form-control" type ="hidden" name="cateid" id="cateid"></input>
						  </div>
						
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Confirm</button>
					  </div>
					  </form>
					</div>
				  </div>
				</div>
	<!-- /#Message BOx -->
	
	
	<!-- /#Category Update BOx -->
	<div class="modal fade" id="categoryUpdateModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Modify a Category</h4>
		  </div>
		  <div class="modal-body">
			<form name="update" action="./push/project-category-update.php" method="post" enctype="multipart/form-data">
			  <div class="form-group">
				<label for="recipient-name" class="control-label pull-left">Category Name:</label>
				<input type="text" name="catename" class="form-control" id="catename">
				<input class="form-control" type ="hidden" name="cateid" id="cateid2"></input>
			  </div>
			  <div class="form-group">
					<input type="file" name="fileToUpload" id="fileToUpload">
				</div>
			  <div class="id"><input type="text" class="form-control" style="display:none" id="recipient-id"></div>
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" name="submit" class="btn btn-primary">Save</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	
	<!-- /#Category Update BOx -->
	
	
	<!-- /#Category add BOx -->
	<div class="modal fade" id="categoryAddModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="exampleModalLabel">Add a Category</h4>
		  </div>
		  <div class="modal-body">
			<form class="text-center" action="./push/project-category-push.php" method="post" enctype="multipart/form-data">
			  <div class="form-group">
				<label for="recipient-name" class="control-label">Category Name:</label>
				<input type="text" name="catename" class="form-control" id="recipient-name">
			  </div>
			  <div class="form-group">
					<input type="file" name="fileToUpload" id="fileToUpload">
				</div>
			  <div class="id"><input type="text" class="form-control" style="display:none" id="recipient-id"></div>
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" name="submit" class="btn btn-primary">Save</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<!-- /#Category add BOx -->
	
	

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>


    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>
	
	<!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

	 <!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	$(document).ready(function ()
	{
		$( ".delete" ).click(function() {
		var buttonData = $(this).data("mydata");

		$('#cateid').val($(this).data('myid'));
		$('#aleartLable').text('Confirm delete project category ' + buttonData + '? It will be delete all projects belongs this category' );
		$('#alertModal').modal('show')
		});
		
		  
		$( ".update" ).click(function() {
			var buttonData = $(this).data("mydata");
			$('#catename').val($(this).data('mydata'));
			$('#cateid2').val($(this).data('myid'));
			$('#categoryUpdateModal').modal('show')
		  });
		  
	});
	

	
	</script>
   
	
</body>

</html>
