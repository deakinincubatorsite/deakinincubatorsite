﻿<?php
$flag = "add";
if (isset($_REQUEST['action']))
{
	if($_REQUEST['action'] == "update"){
		$flag = "update";
		if (isset($_REQUEST['newsid'])){
			
			$newsid = $_REQUEST['newsid'];   
			include("../config/config.php");

			$sql = "SELECT * FROM dig_core_news where id = $newsid";
			$result = $con->query($sql);
			if ($result) {
				if($result->num_rows>0){
					while($row =$result->fetch_array() )
					{
						$title = $row['subject'];
						$overview = $row['message'];
						$created = $row['created'];
						$userid = $row['userid'];


					}
				}
			}
		}
	}
	
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

</head>

<body>

    <div id="wrapper">
        <div id="page-wrapper" >

            <div class="container-fluid" >

                <!-- Page Heading -->
                <div class="row"  style="margin-bottom:20px">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php if($flag == "update") echo "News Update"; else "Publish News"; ?>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="../manage/index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i>  <a href="../manage/index.php?id=2">News Manage</a>
                            </li>
							 <li class="active">
                                <i class="fa fa-tasks"></i> Create news
                            </li>

                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row" style="margin-bottom: 25px;">
					<div class="col-lg-12">
						<form name="project" method="post" enctype="multipart/form-data" action="./push/news-push.php"> 
						  <div class="form-group">
							<label for="articleTitel">Title</label>
							<input type="text" class="form-control" name="title" id="articleTitel"  value="<?php if($flag == "update") echo $title; ?>"placeholder="Enter article title">
							<?php if($flag == "update") echo "<input type='hidden' name='newsid' value='$newsid' >"; ?>
							
						  </div>

						  <div class="form-group">
							<label for="exampleTextarea">Content</label>
							<script id="container" name="detail" type="text/plain"  style="width:100%">
								<?php if($flag == "update") echo $overview; else echo "Input your Content......";?>
							</script>
						  </div>
							
						  <button type="submit" class="btn btn-danger" style="width: 150px;">Back</button>
							<button type="submit" class="btn btn-primary pull-right" style="width: 150px;">Publish</button>
						</form>

					</div>
                

                </div>

               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- /#Message BOx -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content panel-danger">
			  <div class="modal-header panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Confirm to delete </h4>
			  </div>
			 
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Confirm</button>
			  </div>
			</div>
		  </div>
		</div>
		<!-- /#Message BOx -->
    <!-- jQuery -->
    <script src="../js/jquery.js"></script>

	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

  
	
	<script type="text/javascript" src="../js/plugins/editor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/plugins/editor/lang/en/en.js"></script>
	<script type="text/javascript" src="../js/plugins/editor/ueditor.all.js"></script>
    <!-- 实例化编辑器 -->
    <script type="text/javascript">
        var ue = UE.getEditor('container');
    </script>

</body>

</html>
