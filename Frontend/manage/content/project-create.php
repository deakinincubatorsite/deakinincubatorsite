<?php
$flag = "add";
if (isset($_REQUEST['action']))
{
	if($_REQUEST['action'] == "update"){
		$flag = "update";
		if (isset($_REQUEST['projectid'])){
			
			$projectid = $_REQUEST['projectid'];   
			include("../config/config.php");

			$sql = "SELECT * FROM dig_core_project where id = $projectid";
			$result = $con->query($sql);
			if ($result) {
				if($result->num_rows>0){
					while($row =$result->fetch_array() )
					{
						$title = $row['fullname'];
						$overview = $row['description'];
						$catagroyid = $row['catagroyid'];
						$path = $row['path'];
						$userid = $row['ownerid'];


					}
				}
			}
		}
	}
	
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

</head>

<body>

    <div id="wrapper">

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                             <?php if($flag == "update") echo "Project Update"; else "Create a Project"; ?>
                        </h1>
                        <ol class="breadcrumb">
                           <li>
                                <i class="fa fa-dashboard"></i>  <a href="../manage/index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-tasks"></i>  <a href="../manage/index.php?id=4">Project Manage</a>
                            </li>
							 <li class="active">
                                <i class="fa fa-tasks"></i> New Project
                            </li>

                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
					<div class="col-lg-12">
						<form name="project" method="post" enctype="multipart/form-data" action="./push/project-push.php"> 
						  <div class="form-group">
							<label for="articleTitel">Title</label>
							<input type="text" class="form-control" name="title" id="articleTitel"  value="<?php if($flag == "update") echo $title; ?>" placeholder="Enter title">
							<?php if($flag == "update") echo "<input type='hidden' name='projectid' value='$projectid' >"; ?>
						  </div>
						
						  <div class="form-group">
							<label for="contacts">Overview</label>
							 <textarea class="form-control" name="overview" id="contacts" value=""rows="3"><?php if($flag == "update") echo $overview; ?></textarea>
						  </div>
						  
						  <div class="form-group">
							<label for="exampleSelect2">Category </label>
							<select multiple class="form-control" name="category" id="exampleSelect2" required="required">
							<?php
									include("../config/config.php");
									$sql = "SELECT * FROM dig_core_project_catagroy";
									$result = $con->query($sql);
									while($row =$result->fetch_array() ){
									   
									   $title = $row['fullname'];
									   $id = $row['id'];
									  
									   if($title != "")
										{
											echo "
												 <option value='$id'>$title</option>
											";
												
										}
									}
								
								?>
							</select>
						  </div>
						  <div class="form-group">
							<label for="Client">PDF</label>
							<input type="file" name="fileToUpload" id="fileToUpload">
						  </div>
						  
						  <button type="submit" class="btn btn-danger" style="width: 150px;">Back</button>
						<button type="submit" class="btn btn-primary pull-right" style="width: 150px;">Publish</button>
						</form>

					</div>
                

                </div>

               

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
   <!-- jQuery -->
    <script src="../js/jquery.js"></script>

	
	
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

  

   
	<script type="text/javascript" src="../js/plugins/editor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="../js/plugins/editor/lang/en/en.js"></script>
	<script type="text/javascript" src="../js/plugins/editor/ueditor.all.js"></script>
    <!-- ?????? -->
    <script type="text/javascript">
        var ue = UE.getEditor('project-contact');
		var ue = UE.getEditor('project-container');
    </script>

</body>

</html>
