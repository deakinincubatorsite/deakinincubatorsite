<!DOCTYPE html>
<html lang="en">

<head>


    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
						<h1 class="page-header">News Manage</h1>
                </div>
				
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-radius: 0px;" >
                        <div class="panel-heading" style="padding: 0px;">
							<a href="index.php?id=21"class="btn btn-primary" style="border-radius: 0px; margin: 0px; background-color: rgb(166, 188, 105); border-color: aliceblue;" type="button" role="button">Publish news</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Create Time</th>
                                       
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    include("../config/config.php");
                                    $sql = "SELECT * FROM dig_core_news";
                                    $result = $con->query($sql);
                                    while($row =$result->fetch_array() ){
                                       //echo ($row[0])."<br>";

                                       $created = $row['created'];
                                       $title = $row['subject'];
                                       $message = $row['message'];
                                       $userid = $row['userid'];
                                       $newsid = $row['id'];
                                       
                                       $id = $row['id'];
                                       
                                        $query = "SELECT firstname FROM dig_core_account_user where id = $userid";
                                        $result2 = $con->query($query);
                                        while($row2 =$result2->fetch_array() ){
                                            $firstname = $row2['firstname'];
                                        }

                                       if($title != "")
                                        {
                                            echo "
                                            <tr class='odd gradeX'>
                                                <td>$title</td>
                                                <td>$firstname</td>
                                                <td>$created </td>
                                                <td class='center'>
                                                    
                                                    <a href='index.php?id=21&action=update&newsid=$newsid' class='btn btn-default btn-xs' role='button'><i class='fa fa-fw fa-cog'></i></a>
                                                    <a href='#' class='btn btn-default btn-xs delete' data-toggle='modal' data-target='#exampleModal' data-myid='$newsid' data-mydata='$title' role='button'><i class='fa fa-fw fa-trash'></i></a>
                                                </td>
                                            </tr>
                                            ";
                                                
                                        }
                                    }
                                
                                ?>
                                   
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

	<!-- /#Message BOx -->
    <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete Alert</h4>
                      </div>
                      <div class="modal-body">
                        <form name="newsdelete" method="POST" action="../manage/push/news-delete.php"> 
                          <div class="form-group">
                            <h4 class="modal-title" id="aleartLable">Confirm delete project </h4>
                            <input class="form-control" type ="hidden" name="newsid" id="newsid"></input>
                          </div>
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Confirm</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
    <!-- /#Message BOx -->
    
   
   <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>


    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function ()
    {
        $( ".delete" ).click(function() {
        var buttonData = $(this).data("mydata");

        $('#newsid').val($(this).data('myid'));
        $('#aleartLable').text('Confirm delete news ' + buttonData + '?' );
        $('#alertModal').modal('show')
        });
        
          
    });
    

    
    </script>

</body>

</html>
