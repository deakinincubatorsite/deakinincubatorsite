<!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="../index.php"><img style="width: 70px;margin-top: -10px;max-width: 70px;"src="../img/icon/dig-logo.svg"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="../project-list.php">Project</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../index.php">News</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../forum/">Forum</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../reg-page.php">Reg</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../login-page.php">Login</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>