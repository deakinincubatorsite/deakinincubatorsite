<?php
session_start(); 
if (isset($_REQUEST['id']))
{
	$projectid = $_REQUEST['id'];	
	include("config/config.php");

	$sql = "SELECT * FROM dig_core_project where id = $projectid";
					$result = $con->query($sql);
					if ($result) {
					 if($result->num_rows>0){
					  while($row =$result->fetch_array() ){
							$title = $row['fullname'];
						   $overview = $row['description'];
						   $categoryid = $row['catagroyid'];
						   $id = $row['id'];
						   $time = $row['timecreated'];
						   $pdf = $row['path'];
						  
					  }
					 }
					}
	$_SESSION['pdf'] = $pdf;


	$query = "SELECT * FROM dig_core_project_catagroy where id = $categoryid";
	$result2 = $con->query($query);
	while($row2 =$result2->fetch_array() ){
	  
	   $catetitle = $row2['fullname'];
	  
	}
								
								
	
}
else
	echo "<script type='text/javascript'>window.location.href = 'project-list.php';</script>";

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Project:Monetisation Strategy: ABC’s Alphabet Safari</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-frontpage.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	 <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <?php require_once('nav-bar.php'); ?>


    <!-- Image Background Page Header -->
    <!-- Note: The background image is set within the business-casual.css file. -->
    <header class="business-header" style='background: transparent url("./img/appdevelopmet.jpg") no-repeat scroll center center;'>
	 <div class="business-header" style="background-color: rgba(0, 0, 0, 0.32); text-align: left;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class=""><?php echo $title; ?></h1>
					<hr>
					
					<h4 class=""><?php echo $catetitle; ?></h4>
                </div>
            </div>
        </div>
        </div>
    </header>

	<section style="padding: 30px;">
		<!-- Page Content -->
		<div class="container">

			

			<div class="row">
				<div class="col-sm-12">
				
					<h4>Project Overview:</h4>
					<hr>
					<p  class="small" style="line-height: 30px;"><?php echo $overview; ?></p>
					
				</div>
				
			</div>
			</br>
			</br>
			</br>
			<div class="row">
				<div class="panel panel-default" style="border-color: white; border-radius: 0px;">
					<div class="panel-heading" style="padding-left: 0px; padding-right: 0px; background-color: white;">
						<h3 class="panel-title">
							<ul class="nav nav-pills nav-justified">
							  <li role="presentation" class="active"><a href="project-detail.php" style="background-color: rgb(166, 188, 105); border-radius: 0px;">Project Description</a></li>
							  <li role="presentation"><a href="project-resources.php?id=<?php echo $id; ?>">Project Resources</a></li>
							  <li role="presentation"><a href="project-discussions.php?id=<?php echo $id; ?>">Discussions</a></li>
							</ul>
						</h3>
					</div>
					<hr>
					<div>
						<div style="height:800px">
						
						<embed  class="col-sm-12" style="height: 800px; padding: 0px;" src="pdf/web/viewer.php"  />
						</div>
					</div>
				</div>
			</div>
		</div>
    <!-- /.container -->
	</section>
	
    
    
    <!-- Footer -->
     <?php require_once('footer.php'); ?>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>
	



</body>

</html>
