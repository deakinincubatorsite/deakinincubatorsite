<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DIG - Deakin Incubator Group</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <?php require_once('nav-bar.php'); ?>


    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
			<div  class="col-lg-9">
				<div class="row">
					<div class="col-lg-12 text-left">
						<h3><?php if (isset($_REQUEST['category'])) echo $_REQUEST['category']; else echo "All Project";  ?></h3>
						<hr class="primary">
					</div>
				</div>

				<div class="row" style="margin: 0px;">
					<?php
					if (isset($_REQUEST['id']))
					{
						include("config/config.php");
						$cateid = $_REQUEST['id'];
						$sql = "SELECT * FROM dig_core_project where catagroyid = $cateid";
						$result = $con->query($sql);
						while($row =$result->fetch_array() ){

						   $title = $row['fullname'];
						   $overview = $row['description'];
						   $categoryid = $row['catagroyid'];
						   $id = $row['id'];
						   $userid = $row['ownerid'];
						   $time = $row['timecreated'];
						$query = "SELECT firstname FROM dig_core_account_user where id = $userid";
						$result2 = $con->query($query);
						while($row2 =$result2->fetch_array() ){
							$firstname = $row2['firstname'];
						}

						   if($title != "")
							{
								echo"
							<div class='col-sm-12 portfolio-item' style='background-color:white;'>
							<div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
								<div class='col-sm-12 carboard-news-title'>
									<a href='project-detail.php?id=$id'><h4>$title</h4></a>
								</div>

								<div class='col-sm-12 carboard-news-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
									<p>$overview</p>

								</div>
								<div style='height:5px'> </div>
								<div class='col-sm-12' style='padding-top: 10px;'>
									<a href='project-detail.php?id=$id'>View Detail..</a>
									<hr class='primary'>
								</div>
								<div class='col-sm-12'>
									<div class='col-sm-6' style='padding-left:0px'>
										<p class='small pull-left'><a href='#'>$firstname</a></p>
									</div>
									<div class='col-sm-6'>
										<p class='small pull-right'>Create Time: $time</p>
									</div>
								</div>

							</div>
						</div>";

							}
						}

					}
					else
					{

						include("config/config.php");
            $sql = "SELECT project.id as id, project.fullname,project.description,project.ownerid, project.timecreated,dig_core_project_catagroy.fullname as category FROM dig_core_project as project,dig_core_project_catagroy where dig_core_project_catagroy.id = catagroyid";
						$result = $con->query($sql);
						while($row =$result->fetch_array() ){

						   $title = $row['fullname'];
						   $overview = $row['description'];
						   $categoryid = $row['category'];
						   $id = $row['id'];
						   $time = $row['timecreated'];

						   if($title != "")
							{
								echo"
							<div class='col-sm-12 portfolio-item' style='background-color:white;'>
							<div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
								<div class='col-sm-12 carboard-news-title'>
									<a href='project-detail.php?id=$id'><h4>$title</h4></a>
								</div>

								<div class='col-sm-12 carboard-news-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
									<p>$overview</p>

								</div>
								<div style='height:5px'> </div>
								<div class='col-sm-12' style='padding-top: 10px;'>
									<a href='project-detail.php?id=$id'>View Detail..</a>
									<hr class='primary'>
								</div>
								<div class='col-sm-12'>
									<div class='col-sm-6' style='padding-left:0px'>
										<p class='small pull-left'><a href='#'>$categoryid</a></p>
									</div>
									<div class='col-sm-6'>
										<p class='small pull-right'>Create Time: $time</p>
									</div>
								</div>

							</div>
						</div>";

							}
						}
					}





					if ($result) {
					 if($result->num_rows>0){
					  while($row =$result->fetch_array() ){
						   //echo ($row[0])."<br>";

					   $title = $row['fullname'];
					   $overview = $row['description'];
					   $categoryid = $row['catagroyid'];
					   $id = $row['id'];


					   if($title != "")
						{
						echo"
						<div class='col-sm-12 portfolio-item' style='background-color:white;'>
						<div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
							<div class='col-sm-12 carboard-news-title'>
								<a href='news-detail.php?id=$id'><h4>$title</h4></a>
							</div>

							<div class='col-sm-12 carboard-news-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
								<p>$overview</p>

							</div>
							<div style='height:5px'> </div>
							<div class='col-sm-12' style='padding-top: 10px;'>
								<a href='news-detail.php?id=$id'>View Detail..</a>
								<hr class='primary'>
							</div>
							<div class='col-sm-12'>
								<div class='col-sm-6' style='padding-left:0px'>
									<p class='small pull-left'><a href='#'>$categoryid</a></p>
								</div>
								<div class='col-sm-6'>
									<p class='small pull-right'>Create Time</p>
								</div>
							</div>

						</div>
					</div>";
						}
					  }
					 }
					}
					?>
				</div>
			</div>

			<div  class="col-lg-3">

				  <!-- Project Category List -->
                <?php require_once('project-category-list.php'); ?>

			</div>



        </div>
    </section>




    <!-- Footer -->
     <?php require_once('footer.php'); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>



    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>

</body>

</html>
