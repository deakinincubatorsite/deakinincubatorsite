<?php
session_start(); 
if (isset($_REQUEST['id']))
{
    $newsid = $_REQUEST['id'];   
    include("config/config.php");

    $sql = "SELECT * FROM dig_core_news where id = $newsid";
                    $result = $con->query($sql);
                    if ($result) {
                     if($result->num_rows>0){
                      while($row =$result->fetch_array() ){
                            $title = $row['subject'];
                           $overview = $row['message'];
                           $created = $row['created'];
                           $id = $row['userid'];
                           
                          
                      }
                     }
                    }
    
}
else
    echo "<script type='text/javascript'>window.location.href = 'project-list.php';</script>";

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/freelancer.min.css" rel="stylesheet">
	
	<!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
 <!-- Navigation -->
    <?php require_once('nav-bar.php'); ?>

 <section>
     <!-- Page Content -->
    <div class="container" style="margin-top: 20px;">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h2><?php echo $title; ?></h2>

                <!-- Author -->
                <p class="lead">
                  
                </p>

                <div class="col-lg-12">
				 <a href="#"><?php echo $id; ?></a>
				   <p class="pull-right" style="font-size: 14px;"s><span class="glyphicon glyphicon-time"></span><?php echo $created; ?></p>
				    <hr>
				</div>

                <!-- Date/Time -->
                

                <hr>

                

                <!-- Post Content -->
                <p class="lead"><?php echo $overview; ?></p>
               
                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="">
                    <h4>Leave a Comment:</h4>
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control" style="border-radius: 0px;"  rows="3"></textarea>
                        </div>
                        <button type="submit" style="border-radius: 0px;" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->
                

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                    <h4>News Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control" style="border-radius: 0px;">
                        <span class="input-group-btn" style="border-radius: 0px;">
                            <button class="btn btn-default" style="font-size:20px"type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                    <h4>New Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category 1</a>
                                </li>
                                <li><a href="#">Category 2</a>
                                </li>
                                <li><a href="#">Category 3</a>
                                </li>
                                <li><a href="#">Category 4</a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                    <!-- /.row -->
                </div>
				
				 <!-- Blog Categories Well -->
                <div class="well" style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                    <h4>Project Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">APP</a>
                                </li>
                                <li><a href="#">Category 2</a>
                                </li>
                                <li><a href="#">Category 3</a>
                                </li>
                                <li><a href="#">Category 4</a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                    <!-- /.row -->
                </div>

               
            </div>

        </div>
        <!-- /.row -->

        <hr>

    </div>
 </section>
 
  
    <!-- Footer -->
     <?php require_once('footer.php'); ?>
	
	
     <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>

</body>

</html>
