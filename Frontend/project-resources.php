﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Project:Monetisation Strategy: ABC’s Alphabet Safari</title>

   <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
	
	 <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  <?php require_once('nav-bar.php'); ?>

    <!-- Image Background Page Header -->
    <!-- Note: The background image is set within the business-casual.css file. -->
    <header class="business-header" style='background: transparent url("./img/appdevelopmet.jpg") no-repeat scroll center center;'>
	 <div class="business-header" style="background-color: rgba(0, 0, 0, 0.32); text-align: left;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="">Monetisation Strategy: ABC’s Alphabet Safari</h1>
					<hr>
					<h5 class="">Deakin Mentors: Sophie McKenzie, Aaron Spence</h5>
					<h5 class="">Client: Maria Nicholas</h5>
					<h5 class="">Category: Game Development</h5>
                </div>
            </div>
        </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
				<div class="col-sm-7">
				
					<h4>Project Overview:</h4>
					<hr>
					<p  class="small">This project is about developing a plan for how gamedesign and interactive components of the app ‘ABC’sAlphabet Safari’ can be appropriately structured so thatfuture developments/additions to be monetised.Currently ABC’s alphabet safari is designed asa free app,however there is strong potential for an innovative andwell planned monetisation strategy to be applied. At thisstage student involvement on this project is largelyfocused on research, design and preparation of amonetisation strategy. The next phase of this work is todevelopment content as planned out in the strategy. </p>
					
				</div>
				<div class="col-sm-5">
					<h4>Project contacts</h4>
					<hr>
					<p class="small">Shaun Bangay( shaun.bangay@deakin.edu.au )</p>
					<p class="small"Greg Bowtell( g.bowtell@deakin.edu.au )</p>
					<p class="small">Sophie McKenzie(sophie.mckenzie@deakin.edu.au)</p>
				</div>
			</div>
		</br>
		</br>
		</br>
		<div class="row">
			
			
			<div class=" getminheight panel panel-default" style="rgb(236, 230, 230); border-radius: 0px;">
				<div class="panel-heading" style="padding-left: 0px; padding-right: 0px; background-color: white;">
					<h3 class="panel-title">
						<ul class="nav nav-pills nav-justified">
						 
						  
						  <li role="presentation"><a href="project-detail.php">Project Description</a></li>
						  <li role="presentation"  class="active"><a href="project-php.html" style="background-color: rgb(166, 188, 105); border-radius: 0px;">Project Resources</a></li>
						  <li role="presentation"><a href="project-discussions.php">Discussions</a></li>
							  
						</ul>
					</h3>
				</div>
				<div class="panel-body">
					<div class="panel panel-default" style="border-radius: 0px;">
					<table class="table">
					<th>General</th><th>Date </th><th>Size</th><th>Actions</th>
					<tr><td>Resource1.zip</td><td>9/9/2016</td><td>3900</td><td>view</td></tr>
					<tr><td>Image.png</td><td>9/9/2016</td><td>128</td><td>view</td></tr>
					<tr><td>code.zip</td><td>9/9/2016</td><td>320000</td><td>view</td></tr>
					</table>
					</div>
					
					<div class="panel panel-default" style="border-radius: 0px;">
					<table class="table">
					<th>Software</th><th>Type</th><th>Size</th><th>Actions</th>
					<tr><td><a href="https://unity3d.com/">UnityLink</a></td><td>Link</td><td></td><td><a href="https://unity3d.com/">view</a></td></tr>
					<tr><td><a href="http://www.yoyogames.com/gamemaker">GameDev Geelong industry panel</a></td><td>Link</td><td></td><td><a href="https://unity3d.com/">view</a></td></tr>
					<tr><td><a href="https://unity3d.com/">UnityLink</a></td><td>Link</td><td></td><td><a href="https://unity3d.com/">view</a></td></tr>
					<tr><td><a href="https://unity3d.com/">UnityLink</a></td><td>Link</td><td></td><td><a href="https://unity3d.com/">view</a></td></tr>
					
					</table>
					</div>
				</div>
				
				<div class="panel-body">
				<p align="center">
					  <button type="submit" style="border-radius: 0px; background-color: rgb(166, 188, 105); color: white; border: 0px none;" class="btn btn-default pull-right"  data-toggle="modal" data-target="#uploadres" role="button">Upload resources</button>
				 </p>
				
				</div>
			</div>
				
		</div>


    </div>
    <!-- /.container -->
	

	
     <?php require_once('footer.php'); ?>
	
	
		<!-- /#Message BOx -->
	<div class="modal fade" id="uploadres" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Upload resources</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Choose a file</label>
					<input type="file">
				</div>
			<br/>
			</div>
			<div class="modal-footer">
				<br/>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Upload</button>
			</div>
		</div>
	  </div>
	</div>
	<!-- /#Message BOx -->
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>
</body>

</html>
