<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reg</title>

   <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

     <!-- Navigation -->
    <?php require_once('nav-bar.php'); ?>

<section>
    <!-- Page Content -->
    <div class="container">

       

        <!-- Title -->
		<br/>
		<br/>
		<div class="col-lg-3">
		</div>
		<div class="col-lg-6">
            <h1>Sign up</h1>
			<hr>
			<form role="form">
			
				<div class="form-group">
					<label class="control-label" for="inputSuccess">Email Address</label>
					<input type="email" class="form-control" id="inputSuccess">
					 <p class="help-block">Email address as your username</p>
				</div>
				<div class="form-group">
					<label class="control-label" for="inputWarning">Password</label>
					<input type="password" class="form-control" id="inputWarning">
				</div>
				<div class="form-group">
					<label class="control-label" for="inputError">Comfirm Password</label>
					<input type="password" class="form-control" id="inputError">
				</div>
				
				<div class="form-group">
					<label>Preferred Name</label>
					<input class="form-control" placeholder="Enter Preferred Name" required>
				</div>
				<div class="form-group">
					<label>First Name</label>
					<input class="form-control" placeholder="Enter First Name">
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input class="form-control" placeholder="Enter Last Name">
				</div>
				<div class="form-group">
					<label>Title</label>
					<select class="form-control">
						<option>Mr</option>
						<option>Mrs</option>
						<option>Dr</option>
						<option>Ms</option>
						<option>Miss</option>
					</select>
				</div>
				 <div class="form-group">
					<label>Deakin ID</label>
					<input class="form-control" type="email">
					<p class="help-block">If you have please input your ID number</p>
				</div>
				  <span><a href="login-page.html"><button type="button" class="btn btn-link">Back to Login</button></a></span>
					 <button type="submit" style="background-color: rgb(166, 188, 105); border-color: rgb(166, 188, 105);" class="btn btn-success btn-block">Sign Up</button>
		 	</form>
		</div>
				<!-- /.col-lg-6 (nested) -->
		<div class="col-lg-3">
		  
		</div>
		
		
    </div>
    <!-- /.container -->
    </section>

 <?php require_once('footer.php'); ?>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/freelancer.min.js"></script>
	

</body>

</html><script type="text/javascript">
	var g_duration = 93;
var g_iisLatency = 1;
var g_requireJSDone = new Date().getTime();
</script>