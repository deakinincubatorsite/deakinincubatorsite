<?php
include('../config/config.php');
session_start(); 



?>
<!DOCTYPE html>
<html lang='en'>

<head>

  <!-- Bootstrap Core CSS -->
  <link href='../vendor/bootstrap/css/bootstrap.min.css' rel='stylesheet'>

  <!-- Theme CSS -->
  <link href='../css/freelancer.min.css' rel='stylesheet'>

  <!-- Custom Fonts -->
  <link href='../vendor/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
  <script src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
  <![endif]-->

</head>

<body id='page-top' class='index'>

  
  <!-- Portfolio Grid Section -->
  <section id='portfolio'>
    <div class='container'>
      
      <div  class="col-lg-9">
        <div class="row">
          <div class="col-lg-12 text-center">
            <div class="col-sm-11 text-center">
              <h2><?php
              
                echo $_SESSION['title']
              
              ?></h2>
            </div>
            <div class="col-sm-1">
              <button type="button" class="btn btn-sm btn-success create" style="margin-top: 10px;" data-mydata="project">+</button>
            </div>
          </div>
        </div>
        <hr class="primary">

        <div class='row' style='margin: 0px;'>
          <div class='col-sm-12 portfolio-item' style='background-color:white;'>
            <div class='col-sm-12' style='' >
              <?php

              function printChildQuestions($parentid,$conn) {
                $sql = "SELECT * FROM dig_core_discussion where parent = $parentid and belong = 'project'";
                $result = $conn->query($sql);
                if ($result) {
                  {
                    while (true) {
                      $row =$result->fetch_array();
                      if(!$row){
                        break;
                      }
                      $title = $row['subject'];
                      $postdate = $row['created'];
                      $detail = $row['message'];
                      $author = $row['userid'];
                      $modified = $row['modified'];
                      $id = $row['id'];
                      if($title != '')
                      {
                        echo "<div class='col-sm-12 portfolio-item' style='background-color:white;'>
                        <div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
                        <div class='col-sm-12 carboard-news-title'>
                        <a href='discussion.php?id=$id'><h4>$title</h4></a>
                        </div>

                        <div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
                        <h5>posted at $postdate</h5>
                        <p>$detail</P>
                        </div>
                        <div style='height:5px'> </div>
                        <div class='col-sm-12'>
                        <a href='discussion.php?id=$id'>Read More</a>
                        <hr class='primary'>
                        </div>
                        <div class='col-sm-12'>
                        <div class='col-sm-8'>
                        <div class='col-sm-6' style='padding-left:0px; margin-top: 21px;'>
                        <div class='col-sm-3'>
                        </div>
                        <div class='col-sm-3'>
                        </div>
                        <div class='col-sm-1'>
                        </div>
                        </div>
                        </div>
                        <div class='col-sm-1'>
                        <img style='width: 50px;' src ='../img/icon/my-avatar.jpg'/>
                        </div>
                        <div class='col-sm-3'>
                        <p>Last post $modified by</P>
                        <p class='small'>$author</p>
                        </div>
                        </div>

                        </div>
                        </div>";
                      }
                    }
                  }
                }
              }
              if(isset($_SESSION['forumid']))
                $topid = $_SESSION['forumid'];
              //$topid = $_REQUEST['id'];
              printChildQuestions($topid,$con);
              ?>
            </div>
          </section>

          <!-- Modal -->
          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form data-toggle="validator" role="form" method="post" action="insert.php">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Start Topic</h5>
                  </div>
                  <div class="form-group" style="padding: inherit;padding: 20px;min-height: 120px;">
                    <div class="form-group">
                      <label class="control-label">topic</label>
                      <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
                    </div>
                    <div class="form-group has-feedback">
                      <label class="control-label">Description</label>
                      <input id="belong" name="belong" type="hidden"/>
                      <input id="parentid" name="parentid" value="<?php echo $topid; ?>" type="hidden"/>
                      <textarea class="form-control" rows="7"  id ="message" name="message" placeholder="Insert Description" required></textarea>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-info" >Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

            <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
            <div class='scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md'>
              <a class='btn btn-primary' href='#page-top'>
                <i class='fa fa-chevron-up'></i>
              </a>
            </div>



            <!-- jQuery -->
            <script src='../vendor/jquery/jquery.min.js'></script>

            <!-- Bootstrap Core JavaScript -->
            <script src='../vendor/bootstrap/js/bootstrap.min.js'></script>

            <!-- Plugin JavaScript -->
            <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>

            <!-- Contact Form JavaScript -->
            <script src='../js/jqBootstrapValidation.js'></script>
            <script src='../js/contact_me.js'></script>

            <!-- Theme JavaScript -->
            <script src='../js/freelancer.min.js'></script>

            <script>
            $(document).ready(function ()
            {
              $( ".create" ).click(function() {
                $('#belong').val($(this).data('mydata'));
                $('#myModal').modal('show')
              });

            });
            </script>

          </body>

          </html>
