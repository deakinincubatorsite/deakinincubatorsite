<?php
include("../config/config.php");
?>
<!DOCTYPE html>
<html lang='en'>

<head>

	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta name='description' content=''>
	<meta name='author' content=''>

  <title>DIG - Deakin Incubator Group</title>

	<!-- Bootstrap Core CSS -->
	<link href='../vendor/bootstrap/css/bootstrap.min.css' rel='stylesheet'>

	<!-- Theme CSS -->
	<link href='../css/freelancer.min.css' rel='stylesheet'>

	<!-- Custom Fonts -->
	<link href='../vendor/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
	<script src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
	<![endif]-->

</head>

<body id='page-top' class='index'>

	<!-- Navigation -->
   <?php require_once('../nav-bar.php'); ?>



	<!-- Portfolio Grid Section -->
	<section id='portfolio' style='padding-top: 26px;min-height: 700px;'>
		<div class='container'>
			<div  class='col-lg-12' >

				<?php
				$count;
				function printChildQuestions($parentid,$conn,$lvl) {
					$lvl++;
					global $count;
					$sql = "SELECT * FROM dig_core_discussion, dig_core_account_user WHERE dig_core_discussion.userid = dig_core_account_user.id and parent = $parentid and belong = 'topic' order by created";
					$result = $conn->query($sql);

					if ($result) {
						{


							while (true) {
								$row = $result->fetch_array();
								if(!$row){
									break;
								}
								$sql2 = "SELECT * FROM dig_core_discussion where id = $parentid and belong = 'topic' order by created";
								$result2 = $conn->query($sql2);
								$row2 = $result2->fetch_array();

								// $css = '';
								// switch ($lvl) {
								// 	case 0:
								// 	case 1:
								// 	$css =  "padding-left: 0px;";
								// 	break;
								// 	case 2:
								// 	$css =  "padding-left: 25px;";
								// 	break;
								// 	case 3:
								// 	$css =  "padding-left: 50px;";
								// 	break;
								// 	case 4:
								// 	$css =  "padding-left: 75px;";
								// 	break;
								// 	case 5:
								// 	$css =  "padding-left: 80px;";
								// 	break;
								// 	default:
								// 	$css =  "padding-left: 80px;";
								// }

								$title = $row['subject'];
								$postdate = $row['created'];
								$detail = $row['message'];
								$parentMessage= $row2['message'];
								$author = $row['firstname'];
								$modified = $row['modified'];
								$id = $row[0];
								if($title != '')
								{
									$count++;
									echo "<div class='row' style='margin: 0px;'>
									<div class='col-lg-12 portfolio-item' style='background-color:white;'>
									<div class='col-lg-2'>
									<div class='row-sm-5'>
									<img style='width: 50px;' src ='../img/icon/my-avatar.jpg'/>
									</div>
									<div>
									<p class='small'>$author</p>
									</div>
									<div>
									</div>
									</div>
									<div class='col-sm-9'>
									<div class='col-sm-12 portfolio-item' style='margin-bottom: 0px;background-color:white;'>
									<div class='col-sm-12' style='  padding: 0px; border-buttom: 1px solid rgba(0, 0, 0, 0.09);' >
									<div class='col-sm-12 carboard-news-title'>
									<a href='topic-detail.php?id=$id'><h5>posted $postdate</h5></a>
									</div>

									<div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); overflow:hidden;'>
									";
									if($parentMessage != ''){
										echo "
										<div style='background-color: rgba(103, 195, 174, 0.20);    padding: 10px;    margin: 10px;    overflow: hidden;'>
										<p>$parentMessage</p>
										</div>";
									}
									echo "
									<p>
									$detail
									</p>
									</div>
									</div>
									</div>

									</div>
									<div class='col-sm-1'>
									<button type='button' class='btn btn-info btn-sm reply' value='$id' data-mydata='$id' name='reply' rel='popover' data-trigger='click' data-placement='left'>reply</button>
									<h2 class='col-sm-1' style='color: rgba(156, 156, 156, 0.4);margin-top: 30px;'>$count#</h2>
									</div>
									</div>
									<div class='col-sm-12'>
									<hr class='primary'>
									</div>
									</div>";
								}
								printChildQuestions($id,$conn,$lvl);
							}
						}
					}
				}
				if(isset($_REQUEST['id'])){
					$topid = $_REQUEST['id'];
					$lvl = 0;
					$count = 0;
					$sql = "SELECT * FROM dig_core_discussion, dig_core_account_user WHERE dig_core_discussion.userid = dig_core_account_user.id and dig_core_discussion.id = $topid and (belong = 'topic' or belong ='project')order by created";
					$result = $con->query($sql);
					if ($result) {
						$count++;
						$row = $result->fetch_array();
						$title = $row['subject'];
						$postdate = $row['created'];
						$detail = $row['message'];
						$author = $row['firstname'];
						$modified = $row['modified'];
						$id = $row[0];
						echo "
						<div class='row'>
						<div class='col-sm-12 text-left'>
						<h3>$title</h3>
						<hr class='primary'>
						</div>
						</div>
						<div class='row' style='margin: 0px;'>
						<div class='col-lg-12 portfolio-item' style='background-color:white;'>
						<div class='col-lg-2'>
						<div class='row-sm-5'>
						<img style='width: 50px;' src ='../img/icon/my-avatar.jpg'/>
						</div>
						<div>
						<p class='small'>$author</p>
						</div>
						</div>
						<div class='col-sm-9'>
						<div class='col-sm-12 portfolio-item' style='margin-bottom: 0px;background-color:white;'>
						<div class='col-sm-12' style='  padding: 0px; border-buttom: 1px solid rgba(0, 0, 0, 0.09);' >
						<div class='col-sm-12 carboard-news-title'>
						<a href='topic-detail.php?id=$id'><h5>posted $postdate</h5></a>
						</div>

						<div class='col-sm-12 carboard-topic-detail' style='color: rgb(53, 52, 52); overflow:hidden;'>
						<p>
						$detail
						</p>
						</div>

						</div>
						</div>

						</div>
						<div class='col-sm-1'>
						<button type='button' class='btn btn-info btn-sm reply' value='$id' data-mydata='$id' name='reply' rel='popover' data-trigger='click' data-placement='left'>reply</button>
						<h2 class='col-sm-1' style='color: rgba(156, 156, 156, 0.4);margin-top: 30px;'>$count#</h2>
						</div>
						</div>
						<div class='col-sm-12'>
						<hr class='primary'>
						</div>
						</div>";
						printChildQuestions($topid,$con,$lvl);
					}
				}
				?>

			</div>
		</div>
	</section>


	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<form data-toggle="validator" name="replyForm" role="form" method="post" action="insert.php">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Reply</h5>
					</div>
					<div class="form-group" style="padding: inherit;padding: 20px;min-height: 120px;">
						<input id="parentid" name="parentid" type="hidden"/>
						<textarea class="form-control" rows="5"  id ="message" name="message"placeholder="Insert message" required></textarea>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-info" >Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Footer -->
	 <?php require_once('../footer.php'); ?>

		<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
		<div class='scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md'>
			<a class='btn btn-primary' href='#page-top'>
				<i class='fa fa-chevron-up'></i>
			</a>
		</div>

		<!-- jQuery -->
		<script src='../vendor/jquery/jquery.min.js'></script>

		<!-- Bootstrap Core JavaScript -->
		<script src='../vendor/bootstrap/js/bootstrap.min.js'></script>

		<!-- Plugin JavaScript -->
		<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>

		<!-- Contact Form JavaScript -->
		<script src='../js/jqBootstrapValidation.js'></script>
		<script src='../js/contact_me.js'></script>

		<!-- Theme JavaScript -->
		<script src='../js/freelancer.min.js'></script>

		<script>
		$(document).ready(function ()
		{
			$( ".reply" ).click(function() {
				$('#parentid').val($(this).data('mydata'));
				$('#myModal').modal('show')
			});

		});
		</script>

	</body>

	</html>
