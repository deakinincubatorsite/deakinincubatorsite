<?php
include("../config/config.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>DIG - Deakin Incubator Group</title>

	<!-- Bootstrap Core CSS -->
	<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="../css/freelancer.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body id="page-top" class="index">

	<!-- Navigation -->
     <?php require_once('../nav-bar.php'); ?>



	<!-- Portfolio Grid Section -->
	<section id="portfolio">
		<div class="container">
			<div  class="col-lg-3">

				  <!-- Project Category List -->
                <?php require_once('../project-category-list.php'); ?>


			</div>
			<div  class="col-lg-9">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="col-sm-11 text-center">
							<h2>Forum</h2>
						</div>
						<div class="col-sm-1">
							<button type="button" class="btn btn-sm btn-success create" style="margin-top: 10px;" data-mydata="forum">+</button>
						</div>
					</div>
				</div>
				<hr class="primary">

				<div class="row" style="margin: 0px;">


					<?php
					$sql = "SELECT * FROM dig_core_discussion where belong = 'forum' order by created limit 6 ";
					$result = $con->query($sql);
					if ($result) {
						if($result->num_rows>0){
							while($row =$result->fetch_array() ){
								//echo ($row[0])."<br>";

								$title = $row['subject'];
								$postdate = $row['created'];
								$detail = $row['message'];
								$author = $row['userid'];
								$id = $row['id'];
								if($title != "")
								{
									echo"
									<div class='col-sm-12 portfolio-item' style='background-color:white;'>
									<div class='col-sm-12' style='padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >

									<div>
									<div class='col-sm-1 carboard-news-title' style='margin-top: 22px;'>
									<img style='margin-left: 15px;width: 28px;' src ='../img/icon/discussion.svg'/>
									</div>

									<div class='col-sm-6 carboard-news-title'>
									<a href='topic.php?id=$id'><h3>$title</h3></a>
									</div>
									<div class='col-sm-5' style='margin-top: 26px;'>
									<p class='small pull-right'>$postdate</p>
									</div>
									</div>
									<div class='col-sm-12 carboard-news-detail' style='padding: 25px;color: rgb(53, 52, 52); '>
									<p>$detail</p>
									</div>

									</div>
									</div>";
								}
							}
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<form data-toggle="validator" role="form" method="post" action="insert.php">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Create forum</h5>
				</div>
				<div class="form-group" style="padding: inherit;padding: 20px;min-height: 120px;">
					<div class="form-group">
						<label class="control-label">Subject</label>
						<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
					</div>
					<div class="form-group has-feedback">
						<label class="control-label">Description</label>
						<input id="belong" name="belong" type="hidden"/>
						<textarea class="form-control" rows="3"  id ="message" name="message" placeholder="Insert Description" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-info" >Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Footer -->
 <?php require_once('../footer.php'); ?>

	<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
	<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
		<a class="btn btn-primary" href="#page-top">
			<i class="fa fa-chevron-up"></i>
		</a>
	</div>



	<!-- jQuery -->
	<script src="../vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="../js/jqBootstrapValidation.js"></script>
	<script src="../js/contact_me.js"></script>

	<!-- Theme JavaScript -->
	<script src="../js/freelancer.min.js"></script>
	<script>
	$(document).ready(function ()
	{
		$( ".create" ).click(function() {
			$('#belong').val($(this).data('mydata'));
			$('#myModal').modal('show')
		});

	});
	</script>
</body>

</html>
