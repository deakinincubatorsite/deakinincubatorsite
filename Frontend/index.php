<?php
include("config/config.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>DIG - Deakin Incubator Group</title>

	<!-- Bootstrap Core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="css/freelancer.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-social.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body id="page-top" class="index">

	<!-- Navigation -->
	<?php require_once('nav-bar.php'); ?>

	<!-- Header -->
	<header style="background: #a6bc69;">
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
				</div>
				<div class="col-lg-8">

					<div class="intro-text">

						<div class="form-group col-xs-11 floating-label-form-group controls" style="border-left: 0px none; padding-bottom: 0px;">

							<input class="form-control fa fa-search" id="search" style="color: white; font-size: 1.4em;">

							<p class="help-block text-danger" style="margin: 0px;"></p>
						</div>
						<div class="form-group col-xs-1 floating-label-form-group controls" style="border-left: 0px none;margin-top: 3px;">
							<button  style="border:1px solid transparent; background-color: transparent;"><i class="fa fa-search" style="font-size: 1.3em;" aria-hidden="true"></i></button>
						</div>

					</div>
				</div>
				<div class="col-lg-2">
				</div>
			</div>
		</div>
	</header>

	<!-- Portfolio Grid Section -->
	<section id="portfolio" style="min-height: 600px;">
		<div class="container">
			<div  class="col-lg-9">
				<div class="row">
					<div class="col-lg-12 text-left">
						<h3>News</h3>
						<hr class="primary">
					</div>
				</div>

				<div class="row" style="margin: 0px;">

					<?php
					$sql = "SELECT * FROM dig_core_news order by created limit 6";
					$result = $con->query($sql);
					if ($result) {
						if($result->num_rows>0){
							while($row =$result->fetch_array() ){
								//echo ($row[0])."<br>";

								$title = $row['subject'];
								$postdate = $row['created'];
								$price = $row['message'];
								$detail = $row['message'];
								$author = $row['userid'];
								$id = $row['id'];


								$query = "SELECT firstname FROM dig_core_account_user where id = $author";
								$result2 = $con->query($query);
								while($row2 =$result2->fetch_array() ){
									$firstname = $row2['firstname'];
								}


								if($title != "")
								{
									echo"
									<div class='col-sm-12 portfolio-item' style='background-color:white;'>
									<div class='col-sm-12' style='  padding: 0px; border: 1px solid rgba(0, 0, 0, 0.09);' >
									<div class='col-sm-12 carboard-news-title'>
									<a href='news-detail.php?id=$id'><h3>$title</h3></a>
									</div>

									<div class='col-sm-12 carboard-news-detail' style='color: rgb(53, 52, 52); max-height:150px; overflow:hidden;'>
									<p>$detail</p>

									</div>
									<div style='height:5px'> </div>
									<div class='col-sm-12' style='padding-top: 10px;'>
									<a href='news-detail.php?id=$id'>Read More..</a>
									<hr class='primary'>
									</div>
									<div class='col-sm-12'>
									<div class='col-sm-6' style='padding-left:0px'>
									<p class='small pull-left'><a href='#'>$firstname</a></p>
									</div>
									<div class='col-sm-6'>
									<p class='small pull-right'>$postdate</p>
									</div>
									</div>

									</div>
									</div>";
								}
							}
						}
					}
					?>

				</div>
			</div>

			<div  class="col-lg-3">


				<!-- Project Category List -->
				<?php require_once('project-category-list.php'); ?>

			</div>



		</div>
	</section>




	<!-- Footer -->
	<!-- <footer class="text-center">
	<div class="footer-above" style="color: rgb(245, 245, 245);background: hsl(78, 26%, 10%);">
	<div class="container">
	<div class="row">
	<div class="footer-col col-md-3">
	<h3>Location</h3>
	<p>3481 Melrose Place
	<br>Beverly Hills, CA 90210</p>
</div>
<div class="footer-col col-md-6">
<h3>Around the Web</h3>

</div>
<div class="footer-col col-md-2" style="width:200px;height:200px;overflow:hidden">
<img src="img/icon/deakin-logo.svg" class="img-responsive img-centered" alt="">
</div>

</div>

</div>
<hr class="primary" style="border-top: 1px solid rgb(191, 200, 171);">
<div class="row">
<div class=" col-lg-12">
Copyright &copy; Deakin Incubator Groupy 2016
</div>
</div>
</div>

</footer> -->

<footer class="text-center" style="color: rgb(245, 245, 245);background: hsl(78, 26%, 10%);">

	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-4">
				<ul class="list-inline" style="margin-top: 20px;">
					<li class=""><a class ="btn btn-social-icon btn-bitbucket" href="https://bitbucket.org/deakinincubatorsite/deakinincubatorsite"><i class="fa fa-bitbucket"></i></a></li>
					<li class=""><a class ="btn btn-social-icon btn-facebook" href="https://www.facebook.com/DeakinUniversity"><i class="fa fa-facebook"></i></a></li>
					<li class=""><a class ="btn btn-social-icon btn-twitter" href="https://twitter.com/deakin"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
			<div class="col-sm-4">
				<p  style="margin-top: 30px;">Copyright (c) 2016, Deakin Incubator Groupy </p>
			</div>
			<div class="col-sm-4">
				<img src="img/icon/deakin-logo.svg" class="img-responsive img-centered" alt="" style="max-width: 60px;">
			</div>
		</div>
	</div>

</footer>

	<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
	<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
		<a class="btn btn-primary" href="#page-top">
			<i class="fa fa-chevron-up"></i>
		</a>
	</div>



	<!-- jQuery -->
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>
	<!-- Theme JavaScript -->
	<script src="js/freelancer.min.js"></script>

</body>

</html>
